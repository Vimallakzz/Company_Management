package com.ideas2it.address.service;

import java.util.List;
import java.util.Set;

import org.json.simple.JSONObject;

import com.ideas2it.address.model.Address;
import com.ideas2it.exception.DaoException;


/**
 * AddressService interface provides method to add update address for client/
 * and Address.
 * @author Vimalraj J
 * @date 31/08/2017
 */
public interface AddressService {
    
   /**
    * addAddressDetails method gets address details of user stores in pojo and
    * send over Dao layer to store in Database
    *
    * @param permanentAddress - contains permanent address details of Employee
    * @param currentAddress - contains Temporary address details of Employee
    * @param objectType - refers wheather it is  employee or client object type
    *
    * @throws DaoException - it's acustom Exception, occurs while Database 
    * Manipulation
    */
    void addAddressDetails(JSONObject permanentAddress, 
                           JSONObject currentAddress, Object objectType) 
                                                            throws DaoException;
                                 
   /**                             
    * getAddressListByIdAndType method provides list of address information 
    * based on Id and type. 
    * Note: type refers Employee or Client .This type used to get address 
    * details of Employee or client.
    *
    * @param objectType refers Employee or Client object.
    * @param code is used to get address details of particular Employee or 
    * client.
    *
    * @throws DaoException - it's acustom Exception, occurs while Database 
    * Manipulation
    *
    * @return it returns the list of address of particular type of object.
    */ 
    Set<Address> getAddressListByType(Object objectType) throws DaoException;
      
   /**
    * updateAddress method tranfers the address pojo object to Dao layer to 
    * update the address fields of paricular customer or client using 
    * which object is stored in Address Pojo.
    *
    * @param address contains address details of paricular customer or 
    * client using which object is stored in Address Pojo.
    *
    * @throws DaoException - it's acustom Exception, occurs while Database 
    * Manipulation
    *
    * @return returns true when all the employee/client details are properly 
    * updated otherwise returns false 
    */
    void updateAddress(Address address) throws DaoException;    
}


