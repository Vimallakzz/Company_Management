package com.ideas2it.address.service.impl;

import java.util.List;
import java.util.Set;

import org.json.simple.JSONObject;

import com.ideas2it.address.dao.AddressDao;
import com.ideas2it.address.dao.impl.AddressDaoImpl;
import com.ideas2it.address.model.Address;
import com.ideas2it.address.service.AddressService;
import com.ideas2it.client.model.Client;
import com.ideas2it.employee.model.Employee;
import com.ideas2it.exception.DaoException;

/**
 * AddressServiceImpl class implementing AddressService interface.It serves  
 * crud operations(create/read/update) on Address details.
 * @Author Vimalraj.J
 * @Date 29/08/2017
 */
public class AddressServiceImpl implements AddressService {

    private AddressDao addressDao = new AddressDaoImpl();
    
   /**
    * @see com.ideas2it.address.service.AddressService 
    * #method addAddressDetails
    */
    public void addAddressDetails(JSONObject permanentAddress, 
                                  JSONObject currentAddress, 
                                  Object objectType) throws DaoException {
        boolean status = true;
        if (permanentAddress == currentAddress ) {
            this.setAddress(permanentAddress, objectType);
        } else {
            this.setAddress(permanentAddress, objectType);
            this.setAddress(currentAddress, objectType); 
        }
    }   
    
   /**
    * setAddress method sets the set the address detail in Address Pojo and 
    * finds the whether it is Employee details or client details using object
    * type parameter.
    * @param addressDetail - contains address detail of Employee or client 
    * and stored in in Address pojo object.
    * @param objectType - contains Employee or client details based on type 
    * of object.
    */
    private void setAddress(JSONObject addressDetail, Object objectType) 
                                                           throws DaoException {
        String flatNo = (String) addressDetail.get("flatNo");
        String streetName = (String) addressDetail.get("streetName");
        String city = (String) addressDetail.get("city");
        String state = (String) addressDetail.get("state");
        String zipCode = (String) addressDetail.get("zipCode");
        boolean isPermanentAddress = (boolean) addressDetail.get("isPermanent");
        Address address = new Address(flatNo, streetName, city, state, zipCode);
        address.setIsPermanent(isPermanentAddress);
        if (objectType instanceof Employee) {
            address.setEmployee((Employee)objectType);
        } else if (objectType instanceof Client) {
            address.setClient((Client)objectType);
        }
        addressDao.insertAddress(address);
    }
   
   /**
    * @see com.ideas2it.address.service.AddressService 
    * #method getAddressListByIdAndType
    */
    public Set<Address> getAddressListByType(Object objectType) 
                                                           throws DaoException {
        return addressDao.getAddressListByType(objectType);
    }
    
   /**
    * @see com.ideas2it.address.service.AddressService 
    * #method updateAddress
    */
    public void updateAddress(Address address) throws DaoException {
        addressDao.updateAddressDetail(address);
    }
    
   
 
    
}
