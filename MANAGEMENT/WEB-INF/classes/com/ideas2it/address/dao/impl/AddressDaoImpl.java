package com.ideas2it.address.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.ideas2it.address.dao.AddressDao;
import com.ideas2it.address.model.Address;
import com.ideas2it.client.model.Client;
import com.ideas2it.common.ConnectionFactory;
import com.ideas2it.common.Constants;
import com.ideas2it.employee.model.Employee;
import com.ideas2it.exception.DaoException;
import com.ideas2it.logger.ApplicationLogger;

/**
 * AddressDaoImpl class provides Crud operation on Address details of 
 * Employee/client.
 * @author Vimalraj 
 * @Date 06/09/2017
 */
public class AddressDaoImpl implements AddressDao {
    
    static {
        ApplicationLogger.setLoggerInstance();
    }
    
   /**
    * @see com.ideas2it.address.dao.AddressDao
    * #method addAddress(Address address)
    */
    public void insertAddress(Address address) throws DaoException {
        String code = Constants.EMPTY_STRING;
        String flatNo = address.getFlatNo();
        String streetName = address.getStreetName();
        String city = address.getCity();
        String state = address.getState();
        String zipCode = address.getZipCode();
        boolean isPermanent = address.getIsPermanent();
        
        String query = "insert into Address(%s,FlatNo,StreetName,City,State,"+
                       "ZipCode,IsPermanent) values(?,?,?,?,?,?,?)";
        if (address.getEmployee() != null) {
            query = String.format(query,"EmpCode");
            Employee employee = address.getEmployee();
            code = employee.getEmpCode();
        } else if (address.getClient() != null) {
            query = String.format(query,"ClientCode");
            Client client = address.getClient();
            code = client.getClientCode();
        }
        Connection connection = null;
        PreparedStatement prepStatement = null;
        try {
            connection = ConnectionFactory.getConnection();
            prepStatement = connection.prepareStatement(query);
            prepStatement.setString(1,code);
            prepStatement.setString(2,flatNo);
            prepStatement.setString(3,streetName);
            prepStatement.setString(4,city);
            prepStatement.setString(5,state);
            prepStatement.setString(6,zipCode);
            prepStatement.setBoolean(7,isPermanent);
            prepStatement.executeUpdate();
        } catch (SQLException ex){
             ApplicationLogger.error("Error Occured while Inserting Address "+
                  "Detail of Employee/Client whose Unique code  : "+ code , ex);
			throw new DaoException("Something went Wrong ...Please try again "+
                                   "Later...");
        } finally {
            ConnectionFactory.closeConnection(connection,prepStatement);
        }
    }

   /**
    * @see com.ideas2it.address.dao.AddressDao
    * #method getAddressListByIdAndType
    */    
    public Set<Address> getAddressListByType(Object objectType)
                                                          throws DaoException {
        String code = Constants.EMPTY_STRING;
        Connection connection = null;
        PreparedStatement prepStatement = null;
        ResultSet resultSet = null;
        Address address = null;
        Set<Address> employeeAddresses = new HashSet<Address>();
        String query = "select FlatNo, StreetName, City, State, ZipCode, "+
                             "IsPermanent from Address where %s = ?";
        if (objectType instanceof Employee) {
            query = String.format(query,"EmpCode");
            code = ((Employee)objectType).getEmpCode();
        } else if (objectType instanceof Client) {
            query = String.format(query,"ClientCode");
            code = ((Client)objectType).getClientCode();
        }
        try {
            connection = ConnectionFactory.getConnection();       
            prepStatement = connection.prepareStatement(query);
            prepStatement.setString(1,code);
            resultSet = prepStatement.executeQuery();
            while (resultSet.next()) {
                String flatNo = resultSet.getString("FlatNo");
                String streetName = resultSet.getString("StreetName");
                String city = resultSet.getString("City");
                String state = resultSet.getString("State");
                String zipCode = resultSet.getString("ZipCode");
                boolean isPermanent = resultSet.getBoolean("IsPermanent");
                address = new Address(flatNo, streetName, city, state, zipCode);
                address.setIsPermanent(isPermanent);
                if (objectType instanceof Employee) {
                    address.setEmployee((Employee)objectType);
                } else if (objectType instanceof Client) {
                    address.setClient((Client)objectType);
                }
            }
            employeeAddresses.add(address);            
        } catch(SQLException ex) {
            ApplicationLogger.error("Error Occured while Getting Address "+
                  "Detail of Employee/Client whose Unique code  : "+ code , ex);
			throw new DaoException("Something went Wrong ...Please try again "+
                                   "Later...");
        } finally {
            ConnectionFactory.closeConnection(connection,prepStatement);
        }
        return employeeAddresses;
    }
    
   /**
    * @see com.ideas2it.address.dao.AddressDao
    * #method addAddress(Address address)
    */
    public void updateAddressDetail(Address address) throws DaoException {
        String code = Constants.EMPTY_STRING;
        String flatNo = address.getFlatNo();
        String streetName = address.getStreetName();
        String city = address.getCity();
        String zipCode = address.getZipCode();
        String state = address.getState();
        boolean isPermanent = address.getIsPermanent();
        String query = "update Address set FlatNo = ? , StreetName = ?, "+
            "City = ?, State = ?, ZipCode = ? where %s = ? and IsPermanent = ?";
        if (address.getEmployee() != null) {
            query = String.format(query,"EmpCode");
            Employee employee = address.getEmployee();
            code = employee.getEmpCode();
        } else if (address.getClient() != null) {
            query = String.format(query,"ClientCode");
            Client client = address.getClient();
            code = client.getClientCode();
        }
        Connection connection = null;
        PreparedStatement prepStatement = null;
        try {
            connection = ConnectionFactory.getConnection();
            prepStatement = connection.prepareStatement(query);
            prepStatement.setString(1,flatNo);
            prepStatement.setString(2,streetName);
            prepStatement.setString(3,city);
            prepStatement.setString(4,state);
            prepStatement.setString(5,zipCode);
            prepStatement.setString(6,code);
            prepStatement.setBoolean(7,isPermanent);
            prepStatement.executeUpdate();
        } catch (SQLException ex) {
             ApplicationLogger.error("Error Occured while Updating Address "+
                "Details of Client/Employee  whose Unique code : " + code , ex);
			throw new DaoException("Something went Wrong ...Please try again "+
                                   "Later..."); 
        } finally {
            ConnectionFactory.closeConnection(connection,prepStatement);
        }
    }
    
}
