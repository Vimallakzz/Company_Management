package com.ideas2it.address.dao;

import java.util.List;
import java.util.Set;

import com.ideas2it.address.model.Address;
import com.ideas2it.exception.DaoException;

/**
 * AddressDao interface provides necessarry methods which needs to implement 
 * Address crud operation .
 * @author Vimalraj.J
 * @Date 6/09/2017
 */
public interface AddressDao {
    
   /**
    * insertAddress method insert address details of customer or client in 
    * database
    *
    * @param address - contains Address details of Customer or client in 
    * Address pojo.
    *
    * @throws DaoException - It's a custom Exception ,occurs while Database 
    * Manipulation
    */
    void insertAddress(Address address) throws DaoException;
    
   /**                             
    * getAddressListByIdAndType method provides list of address information 
    * based on Id and type. 
    * Note: type refers Employee or Client .This type used to get address 
    * details of Employee or client.
    *
    * @param type refers Employee or Client object.
    * @param code is used to get address details of particular Employee or 
    * client.
    *
    * @throws DaoException - It's a custom Exception ,occurs while Database 
    * Manipulation
    *
    * @return it returns the list of address of particular type of object.
    */ 
    Set<Address> getAddressListByType(Object obj) throws DaoException;
    
   /**
    * updateAddressDetail method update the address fields of paricular customer 
    * or client using which object is stored in Address Pojo.
    *
    * @param address contains address details of paricular customer or 
    * client using which object is stored in Address Pojo.
    *
    * @throws DaoException - It's a custom Exception ,occurs while Database 
    * Manipulation.
    */
    void updateAddressDetail(Address address) throws DaoException;
}
