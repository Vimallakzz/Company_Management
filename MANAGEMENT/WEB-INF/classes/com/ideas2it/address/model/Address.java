package com.ideas2it.address.model;

import com.ideas2it.client.model.Client;
import com.ideas2it.employee.model.Employee;

/**
 * Address pojo class provides setters and getters to store address fields for
 * Employee and client address.
 * @author Vimalraj.J
 * @Date 5-09-2017
 */
public class Address {
    private Employee employee;
    private Client client;
    private int id;
    private String flatNo;
    private String streetName;
    private String city;
    private String state;
    private String zipCode;
    private boolean isPermanent;
    
    public Address() {
    
    }
    
    public Address(String flatNo,String streetName,String city,String state,
                                                            String zipCode) {
       this.flatNo = flatNo;
       this.streetName = streetName;
       this.city = city;
       this.state = state;
       this.zipCode = zipCode;                                                    
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
  
    public void setClient(Client client) {
        this.client = client;
    }

    public void setFlatNo(String flatNo) {
        this.flatNo = flatNo;
    }
    
    public void setStreetName(String streetName){
        this.streetName = streetName;
    }

    public void setCity(String city) {
        this.city = city;
    }
    
    public void setState(String state) {
        this.state = state;
    }
    
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
    
    public void setIsPermanent(boolean isPermanent) {
        this.isPermanent = isPermanent;
    }
    
    public int getId() {
        return this.id;
    }
    
    public Employee getEmployee() {
        return this.employee;
    }
    
    public Client getClient() {
        return this.client;
    }
    
    public String getFlatNo() {
        return this.flatNo;
    }
    
    public String getStreetName(){
        return this.streetName;
    }

    public String getCity() {
        return this.city;
    }
    
    public String getState() {
        return this.state;
    }
    
    public String getZipCode() {
        return this.zipCode;
    }
    
    public boolean getIsPermanent() {
        return this.isPermanent;
    }
    
    public String toString() {
        String str = "FlatNo : "+ this.flatNo + "\nStreetName :" + 
                      this.streetName + "\nCity :" + this.city + "\nState : " + 
                      this.state + "\nZipcode :" + this.zipCode +
                      "\nIsPermanent : "+this.isPermanent;
        return str; 
    }
    
}


