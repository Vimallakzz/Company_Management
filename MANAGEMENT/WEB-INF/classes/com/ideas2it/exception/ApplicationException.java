package com.ideas2it.exception;

import java.lang.Exception;


/**
 * ApplicationException class is custom Exception class used to intimate the  
 * user data exception occurs in Dao layer 
 * @author Vimalraj.j
 * @date 29/08/2017
 */
public class ApplicationException extends Exception {


    public ApplicationException() {
        super();
    } 
    
    public ApplicationException(String message) {
        super(message);
    } 
}
