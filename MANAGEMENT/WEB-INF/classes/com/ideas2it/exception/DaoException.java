package com.ideas2it.exception;

import java.lang.Exception;

/**
 * DaoException class is custom Exception class used to intimate the database 
 * exception occurs in Dao layer 
 * @author Vimalraj.j
 * @date 29/08/2017
 */
public class DaoException extends Exception {

    public DaoException() {
        super();
    } 
    
    public DaoException(String message) {
        super(message);
    } 
    
    public DaoException(String message,Throwable cause) {
        super(message,cause);
    } 
    
    public DaoException(Throwable cause) {
        super(cause);
    } 
}
