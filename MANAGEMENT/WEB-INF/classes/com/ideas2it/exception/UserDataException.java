package com.ideas2it.exception;

import java.lang.Exception;

import org.json.simple.JSONObject;

/**
 * UserDataException class is custom Exception class used to intimate the  
 * user data exception occurs in Dao layer 
 * @author Vimalraj.j
 * @date 29/08/2017
 */
public class UserDataException extends Exception {

    private JSONObject errorFields ;

    public UserDataException() {
        super();
    } 
    
    public UserDataException(String message) {
        super(message);
    } 
    
    public UserDataException(String message,JSONObject errorFields) {
        super(message);
        this.errorFields = errorFields;
    }
    
    public JSONObject getErroredUserData() {
        return this.errorFields;
    }
}
