package com.ideas2it.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.simple.JSONObject;

import com.ideas2it.common.Constants;
import com.ideas2it.common.ErrorCodes;
import com.ideas2it.employee.model.Employee;
import com.ideas2it.project.model.Project;

/**
 * ValidationUtil class provides validation of employee and project fields. if 
 * particular field found to error,then error message is added to respective 
 * fields in json object.
 * @author Vimalraj.J 
 * @Date 29/08/2017
 */

public class ValidationUtil {

    private JSONObject errorFields = null;

   /**
    * validate method finds which type of object is passed and redirects to 
    * particular method
    * @param obj of Object type
    * @return it returns json object with error message to respective error 
    * fields.

	public JSONObject validate(Object obj) {
		if(obj instanceof Employee)
		{
            Employee employeeDetail = (Employee)obj;
			this.validateEmployeeDetails(employeeDetail);
		}else if(obj instanceof Project) {
            Project projectDetail = (Project)obj;
			this.validateProjectDetails(projectDetail);
		}
     return errorFields;
	} */

   /**
    * validateEmployeeDetails each fields by redirecting to seperate methods
    * @param employeeDetail of type employee object
    */
    public JSONObject validateEmployeeDetails(JSONObject employeeDetail) {
        String name = (String) employeeDetail.get("name");
        String emailId = (String) employeeDetail.get("emailId");
        String phoneNo = (String) employeeDetail.get("phoneNo");
        String dateOfBirth = (String) employeeDetail.get("dateOfBirth");
        String dateOfJoining = (String)employeeDetail.get("dateOfJoining");

        this.validateEmpName(name);   
        this.validateEmailId(emailId);
        this.validateMobileNo(phoneNo);
        this.validateDateOfBirth(dateOfBirth);
        this.validateDateOfJoining(dateOfJoining);
        
        
	 return errorFields;
    }

   /**
    * validateProjectDetails each fields by redirecting to seperate methods
    * @param projectDetail of type employee object
    */
    public JSONObject validateProjectDetails(JSONObject projectDetail) {
        String title = (String) projectDetail.get("title");
        String description = (String) projectDetail.get("description");
        String domain = (String) projectDetail.get("domain");

        this.validateProjectName(title);   
        this.validateProjectDescription(description);
        this.validateProjectDomain(domain);
	    return errorFields;
    }

   /**
    * validateEmpName validates Employee Name.If it found error message is 
    * added in json object. 
    * @param name of String type
    */
    @SuppressWarnings("unchecked")    
    public void validateEmpName(String name) {
        String regex = Constants.EMP_NAME_PATTERN;
        boolean status = Pattern.matches(regex,name);
        if(!status) {
            if(errorFields==null) {
                errorFields=new JSONObject();
            }
            errorFields.put("name",ErrorCodes.EMP_NAME);         
        }
    }

   /**
    * validateEmailId validates emailId . If it found error  message is added
    * in json object. 
    * @param emailId of String type 
    */
    @SuppressWarnings("unchecked")
    public void validateEmailId(String emailId) {
        String regex = Constants.EMAILID_PATTERN;
        boolean status = Pattern.matches(regex,emailId);
        if(!status) {
            if(errorFields==null) {
                errorFields=new JSONObject();
            }
            errorFields.put("emailId",ErrorCodes.EMAIL_ID);         
        }
         
    }
    
   /**
    * validateMobileNo validates mobileNo.If it found error error message is 
    * added in json object. 
    * @param mobileNo of String type
    */
    @SuppressWarnings("unchecked")
    public void validateMobileNo(String mobileNo) {
        String regex = Constants.MOBILE_NO_PATTERN;
        boolean status = Pattern.matches(regex,mobileNo);
        if(!status) {
            if(errorFields==null) {
                errorFields=new JSONObject();
            }
            errorFields.put("phoneNo",ErrorCodes.PHONE_NO);         
        }         
    }

   /**
    * validateDateOfBirth method validates dateofBirth. If it found error  
    * message is added in json object. 
    * @param dateOfBirth of String type
    */
    @SuppressWarnings("unchecked")
    public void validateDateOfBirth(String dateOfBirth) {
        String regex = Constants.DATE_PATTERN;
        boolean status = Pattern.matches(regex,dateOfBirth);
        if(!status) {
            if(errorFields==null) {
                errorFields=new JSONObject();
            }
            errorFields.put("dateOfBirth",ErrorCodes.DATE_PATTERN);         
        }         
    }

   /**
    * validateDateOfJoining method validates dateofjoining. If it found error  
    * message is added in json object. 
    * @param dateOfJoining of String type
    */
    @SuppressWarnings("unchecked")    
     public void validateDateOfJoining(String dateOfJoining) {
        String regex = Constants.DATE_PATTERN;
        boolean status = Pattern.matches(regex,dateOfJoining);
        if(!status) {
            if(errorFields==null) {
                errorFields=new JSONObject();
            }
            errorFields.put("dateOfJoining",ErrorCodes.DATE_PATTERN);         
        }      
    }

   /**
    * validateProjectName validates Project Name.  If it found error error 
    * message is added in json object.
    */
    @SuppressWarnings("unchecked")    
    public void validateProjectName(String title) {
        String regex = Constants.PROJ_NAME_PATTERN;
        boolean status = Pattern.matches(regex,title);
        if(!status) {
            if(errorFields==null) {
                errorFields=new JSONObject();
            }
            errorFields.put("title",ErrorCodes.TITLE);         
        }
    }
    
   /**
    * validateProjectDescription validates Description about project.  If it 
    * found error error message is added in json object.
    * @param description of String type
    */
    @SuppressWarnings("unchecked")
    public void validateProjectDescription(String description) {
        String regex = Constants.DESCRIPTION_PATTERN;
        boolean status = Pattern.matches(regex,description);
        if(!status) {
            if(errorFields==null) {
                errorFields=new JSONObject();
            }
            errorFields.put("description",ErrorCodes.DESCRIPTION);         
        }
    }
 
   /**
    * validateProjectDomain validates project domain.  If it found error  
    * message is added in json object. 
    * @param domain of String type
    */
    @SuppressWarnings("unchecked")   
    public void validateProjectDomain(String domain) {
        String regex = Constants.DESCRIPTION_PATTERN;
        boolean status = Pattern.matches(regex,domain);
        if(!status) {
            if(errorFields==null) {
                errorFields=new JSONObject();
            }
            errorFields.put("domain",ErrorCodes.DOMAIN);         
        }
    }
}    


    
