package com.ideas2it.util;

import java.util.Date;

/**
 * DateUtil class provides some common date validations and date manipulations
 * @Author Vimalraj
 * @Date   28/08/2017 
 */
public class DateUtil {
   /**
    * calculateDateDiffInDays methods finds a no of days bw given two dates
    * @param endDate 
    * @param startDate
    * @return it return no of days bw two dates 
    */
    public static int calculateDateDiffInDays(Date endDate, Date startDate) {
        long diffMillis = endDate.getTime() - startDate.getTime();
        int noOfDays = (int)(diffMillis/(1000*60*60*24));
        return noOfDays;
    }

}
