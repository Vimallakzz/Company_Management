package com.ideas2it.project.service;

import java.util.List;

import org.json.simple.JSONObject;

import com.ideas2it.exception.DaoException;
import com.ideas2it.project.model.Project;

/**
 * ProjectService interface provides basic crud operations methods, which needs
 * needs to implement, while implement this interface.
 * @author Vimalraj J
 * @date 29/08/2017
 */
public interface ProjectService {

   /**
    * AddProjectDetails method gets project detail in JSONObject and it stores 
    * the details in Project Pojo and send over DAO layer to store in Database.
    *
    * @param projectDetail contains basic project Details like project Title,
    * Description, Domain in Json object.
    *
    * @throws DaoException - it's acustom Exception, occurs while Database 
    * Manipulation
    *
    * @return  it returns true when Project details are successfully added or 
    * else returns false.
    */
	void addProjectDetails(Project project) throws DaoException;
   
   /**
    * getAllProjectDetails method gets Project details from database as List of
    * Project Model .
    *
    * @throws DaoException - it's acustom Exception, occurs while Database 
    * Manipulation
    *
    * @return - it returns all the Project details as list of Project pojo 
    * Objects
    */
	List<Project> getAllProjectDetails() throws DaoException;

    /**
    * modifyProjectDetails method updates Project records using Project Pojo. 
    * This Object contains Edited Project details. 
    * @param project model Object contains project  details.
    *
    * @throws DaoException - it's acustom Exception, occurs while Database 
    * Manipulation
    *
    * @return returns true when all the Project details are properly updated
    * otherwise returns false 
    */
    @SuppressWarnings("unchecked")
	void modifyProjectDetails(Project project) throws DaoException;

   /**
    * removeProjectById remove a particular employee details by his 'ProjectId'
    * @param ProjectId --- Unique Id to remove  particular project details
    *
    * @throws DaoException - it's acustom Exception, occurs while Database 
    * Manipulation
    *
    * @return it returns true when project details are removed .Otherwise it
    * returns false
    */
	void removeProjectById(String projectCode) throws DaoException;
   
   /**
    * getProjectClientById method gets Project Pojo object of particular ProjectId.
    * @param projectCode - unique Id to refer Project
    *
    * @throws DaoException - it's acustom Exception, occurs while Database 
    * Manipulation
    *
    * @return Project - Project model Object Contains Project details
    */
	Project getProjectClientById(String projectCode) throws DaoException;
	
   /**
    * getProjectModelById method gets Project Pojo object of particular ProjectId.
    * @param projectCode - unique Id to refer Project
    *
    * @throws DaoException - it's acustom Exception, occurs while Database 
    * Manipulation
    *
    * @return Project - Project model Object Contains Project details
    */
	Project getProjectModelById(String projectCode) throws DaoException;
	
	/**
    * getProjectEmployeesById method gets Project Pojo object of particular projectCode.
    * @param projectCode - unique Id to refer Project
    *
    * @throws DaoException - it's acustom Exception, occurs while Database 
    * Manipulation
    *
    * @return Project - Project model Object Contains Project details
    */   
    Project getProjectEmployeesById(String projectCode) throws DaoException;	
}
