package com.ideas2it.project.service.impl;

import java.lang.Boolean;
import java.util.List;

import org.json.simple.JSONObject;

import com.ideas2it.client.model.Client;
import com.ideas2it.client.service.ClientService;
import com.ideas2it.client.service.impl.ClientServiceImpl;
import com.ideas2it.common.Constants;
import com.ideas2it.exception.DaoException;
import com.ideas2it.project.Dao.impl.ProjectDaoImpl;
import com.ideas2it.project.Dao.ProjectDao;
import com.ideas2it.project.model.Project;
import com.ideas2it.project.service.ProjectService;

/**
 * ProjectServiceImpl class implementing ProjectService interface.It serves  
 * crud operations(create/read/update/delete) on Project details.
 * This class stores the list of Project details in POJO objects and it stored 
 * in collection objects to retrive those information.
 * @Author Vimalraj.J
 * @Date 29/08/2017
 */
public class ProjectServiceImpl implements ProjectService {
    private ProjectDao projectDao = new ProjectDaoImpl();
	
   /**
    * @see com.ideas2it.project.service.ProjectService 
    * #method addProjectDetails(String,String,String,String,String)
    */
    public void addProjectDetails(Project project) throws DaoException {
        projectDao.insertProjectDetails(project);
    }
   
   /**
    * @see com.ideas2it.project.service.ProjectService 
    * #method displayProjectDetails()
    */ 
    @SuppressWarnings("unchecked")
    public List<Project> getAllProjectDetails() throws DaoException {
        return projectDao.getAllProjectDetails(); 
    }

   /**
    * @see com.ideas2it.project.service.ProjectService 
    * #method updateProjectDetails(String,JSONObject)
    */   
    @SuppressWarnings("unchecked")
    public void modifyProjectDetails(Project project) throws DaoException {
		projectDao.updateProjectDetails(project);
    }
    
   /**
    * @see com.ideas2it.project.service.ProjectService 
    * #method removeProjectDetails(String)
    */  
    @SuppressWarnings("unchecked")
    public void removeProjectById(String projectCode) throws DaoException {
        projectDao.removeProject(projectCode);
    }
	
	/**
    * @see com.ideas2it.project.service.ProjectService 
    * #method getProjectById
    */
	public Project getProjectModelById(String projectCode) throws DaoException {
		return projectDao.getProjectModelById(projectCode);
	}
	
	/**
    * @see com.ideas2it.project.service.ProjectService 
    * #method getProjectClientById()
    */
	public Project getProjectClientById(String projectCode) throws DaoException {
		return projectDao.getProjectClientById(projectCode);
	}
	
	/**
    * @see com.ideas2it.project.service.ProjectService 
    * #method getProjectEmployeesById()
    */
	public Project getProjectEmployeesById(String projectCode) throws DaoException {
		return projectDao.getProjectEmployeesById(projectCode);
	}
}

