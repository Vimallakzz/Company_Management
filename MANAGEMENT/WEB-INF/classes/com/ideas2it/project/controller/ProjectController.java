package com.ideas2it.project.controller;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import org.json.simple.JSONObject;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;

import com.ideas2it.client.model.Client;
import com.ideas2it.client.service.ClientService;
import com.ideas2it.client.service.impl.ClientServiceImpl;
import com.ideas2it.common.Constants;
import com.ideas2it.employee.model.Employee;
import com.ideas2it.employee.service.EmployeeService;
import com.ideas2it.employee.service.impl.EmployeeServiceImpl;
import com.ideas2it.exception.DaoException;
import com.ideas2it.project.model.Project;
import com.ideas2it.project.service.impl.ProjectServiceImpl;
import com.ideas2it.project.service.ProjectService;
import com.ideas2it.util.ValidationUtil;

/**
 * ProjectController class provides an option for 
 * Crud operations(read/update/delete/create) .
 * @Author Vimalraj
 * @Date   28/08/2017 
 */
public class ProjectController extends HttpServlet {
    private ProjectService projectService = new ProjectServiceImpl();
  
    public boolean isSessionExpired(HttpServletRequest req, 
                  HttpServletResponse res) throws ServletException,IOException {
        HttpSession session =  req.getSession(false);
        if(session != null && session.getAttribute("userName") != null) {
            return true;  
        }
        return false;
    }
    
    public void doGet(HttpServletRequest req,HttpServletResponse res)  
                                           throws ServletException,IOException {
        boolean status = this.isSessionExpired(req,res);
        if(status) {
            String parameter = req.getParameter("parameter");
            if(parameter.equals("addClientProject")) {
                try {
                    ClientService clientService = new ClientServiceImpl();
                    List<Client> clients = clientService.getAllClientDetails();
                    req.setAttribute("clients",clients);
                    RequestDispatcher rd = req.getRequestDispatcher("/jsp/project/addprojectdetails.jsp");
                    rd.forward(req,res);
                } catch (DaoException ex) {
                    System.out.println(ex.getMessage());
                }          
            } else if (parameter.equals("addProjectDetails")) {
                this.addProjectDetail(req,res);
            } else if (parameter.equals("getAllProjects")) {
                this.getAllProjects(req,res);
            } else if (parameter.equals("getProject")) {
                this.getProject(req,res);
            } else if (parameter.equals("updateProjectDetails")) {
                this.updateProject(req,res);
            } else if (parameter.equals("deleteProject")) {
                this.removeProject(req,res);
            } else if (parameter.equals("getProjectEmployees")) {
                    String projectCode = req.getParameter("projectId");
                    try {
                        EmployeeService employeeService = new EmployeeServiceImpl(); 
                        List<Employee> employees = employeeService.getAllEmployeeDetails();  
                        Project project = projectService.getProjectEmployeesById(projectCode);                 
                        req.setAttribute("employees",employees);
                        req.setAttribute("project",project);
                        RequestDispatcher rd = req.getRequestDispatcher("/jsp/project/assignemployees.jsp");
                        rd.forward(req,res);
                    } catch (DaoException ex) {
                        System.out.println(ex.getMessage());
                    }
            } else {
                res.sendError(503);
            }
        } else {
        res.sendRedirect("/MANAGEMENT/html/login.html");
        }
            
    }
    public void doPost(HttpServletRequest req,HttpServletResponse res)  
                                           throws ServletException,IOException {
         boolean status = this.isSessionExpired(req,res);
         if(status) {
            String parameter = req.getParameter("parameter");
            if(parameter.equals("addClientProject")) {
                try {
                    ClientService clientService = new ClientServiceImpl();
                    List<Client> clients = clientService.getAllClientDetails();
                    req.setAttribute("clients",clients);
                    RequestDispatcher rd = req.getRequestDispatcher("/jsp/project/addprojectdetails.jsp");
                    rd.forward(req,res);
                } catch (DaoException ex) {
                    System.out.println(ex.getMessage());
                }          
            } else if (parameter.equals("addProjectDetails")) {
                this.addProjectDetail(req,res);
            } else if (parameter.equals("getAllProjects")) {
                this.getAllProjects(req,res);
            } else if (parameter.equals("getProject")) {
                this.getProject(req,res);
            } else if (parameter.equals("updateProjectDetails")) {
                this.updateProject(req,res);
            } else if (parameter.equals("deleteProject")) {
                this.removeProject(req,res);
            } else if (parameter.equals("getProjectEmployees")) {
                    String projectCode = req.getParameter("projectId");
                    try {
                        EmployeeService employeeService = new EmployeeServiceImpl(); 
                        List<Employee> employees = employeeService.getAllEmployeeDetails();  
                        Project project = projectService.getProjectEmployeesById(projectCode);                 
                        req.setAttribute("employees",employees);
                        req.setAttribute("project",project);
                        RequestDispatcher rd = req.getRequestDispatcher("/jsp/project/assignemployees.jsp");
                        rd.forward(req,res);
                    } catch (DaoException ex) {
                        System.out.println(ex.getMessage());
                    }
            } else {
                res.sendError(503);
            }
        } else {
        res.sendRedirect("/MANAGEMENT/html/login.html");
        }
    }
    
    
   /**
    * AddProject method get input from user . It validates the project details 
    * and sends the project details to service class to add in pojo objects
    */
    @SuppressWarnings("unchecked")
    public void addProjectDetail(HttpServletRequest req,HttpServletResponse res)  
                                           throws ServletException,IOException {
        Project project = this.getProjectDetailsFromRequest(req, res);
        try {
            projectService.addProjectDetails(project);
            req.setAttribute("message","Project Details Added Successfully");
            this.getAllProjects(req,res);
        } catch (DaoException ex) {
            System.out.println(ex.getMessage());
        }
    } 

   /**
    * displayProject method displey the project details before it checks the 
    * Availablity of records    
    */
    public void getAllProjects(HttpServletRequest req,HttpServletResponse res)  
                                           throws ServletException,IOException {
        List<Project> projects;
        try {
            projects = projectService.getAllProjectDetails();
            req.setAttribute("projects", projects);
            RequestDispatcher rd = req.getRequestDispatcher("/jsp/project/displayallprojectdetails.jsp");
            rd.forward(req,res);
        } catch (DaoException ex) {
            System.out.println(ex.getMessage());
        }
    }
    
     private void getProject(HttpServletRequest req, HttpServletResponse res) 
                                           throws ServletException,IOException {
        String action = req.getParameter("action");
        String projectCode = req.getParameter("projectId");
        Project project = null;
        try {
            if(action.equals("display")) {
                project = projectService.getProjectModelById(projectCode);
                req.setAttribute("project", project);
                RequestDispatcher rd = req.getRequestDispatcher("/jsp/project/displayprojectdetails.jsp");
            rd.forward(req,res);
            } else if(action.equals("edit")) {
                project = projectService.getProjectClientById(projectCode);
                req.setAttribute("project", project);
                RequestDispatcher rd = req.getRequestDispatcher("/jsp/project/updateprojectdetails.jsp");
                rd.forward(req,res);
            }
        } catch (DaoException ex) {
            System.out.println(ex.getMessage());
        }                   
    }

   /**
    * updateProject method updates the project details using projectId. Before
    * Updating the project details it validates the error fields and get the 
    * error fields from user to updte the project details.
    */
    @SuppressWarnings("unchecked")
    public void updateProject(HttpServletRequest req,HttpServletResponse res)  
                                           throws ServletException,IOException {
        int id = Integer.parseInt((String)req.getParameter("id"));
        String projectCode = (String) req.getParameter("projectCode");
        try {
            Project project = projectService.getProjectClientById(projectCode);
            String title = req.getParameter("projectTitle");
            String description = req.getParameter("projectDescription");
            String domainValues[] = req.getParameterValues("projectDomain");
            String domain = Constants.EMPTY_STRING;
            for(String str : domainValues) {
                domain = domain + str + " ";
            }
            project.setTitle(title);
            project.setDescription(description);
            project.setDomain(domain);
            projectService.modifyProjectDetails(project);
            req.setAttribute("message","Project Details Updated Successfully");
            this.getAllProjects(req,res);
        } catch (DaoException ex) {
            System.out.println(ex.getMessage());
        } 
    }

   /**
    * removeProject method remove the project details using projectId.Before 
    * it checks availabilty of records
    */
    public void removeProject(HttpServletRequest req,HttpServletResponse res)  
                                           throws ServletException,IOException {   
        String projectCode = req.getParameter("projectId");
        try {
            projectService.removeProjectById(projectCode);
            req.setAttribute("message","Client Details Deleted Successfully");
            this.getAllProjects(req,res);
        } catch (DaoException ex) {
            System.out.println(ex.getMessage());
        } catch (InputMismatchException ex) {
            System.out.println("\n---projectId allows only Integer(0-9)---\n");
        }    
    }

   /**
    * GetProjectDetails method gets project details from user and stores
    * jsonobject.
    *
    * @return it returns the project details which is stored in JSON object
    */
    @SuppressWarnings("unchecked")
    public Project getProjectDetailsFromRequest(HttpServletRequest req,
                                                        HttpServletResponse res)  
                                           throws ServletException,IOException {
        Project project = null;
        Client client = null;
        ClientService clientService = new ClientServiceImpl();
        String clientCode = req.getParameter("clientId");
        String title = req.getParameter("projectTitle");
        String description = req.getParameter("projectDescription");
        String domainValues[] = req.getParameterValues("projectDomain");
        String domain = Constants.EMPTY_STRING;
        for(String str : domainValues) {
            domain = domain + str + " ";
        }
        try { 
            client = clientService.getClientAndAddressById(clientCode);
            project = new Project(client, title, description, domain, 
                                                                  Boolean.TRUE); 
        } catch (DaoException ex) {
            System.out.println(ex.getMessage());
        }      
        return project; 
    }
}
