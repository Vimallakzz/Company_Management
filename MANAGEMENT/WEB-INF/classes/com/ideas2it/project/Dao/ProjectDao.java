package com.ideas2it.project.Dao;

import java.util.List;

import com.ideas2it.exception.DaoException;
import com.ideas2it.project.model.Project;

/*
 * ProjectDao interface provides database crud operations with some necessary 
 * methods 
 * @author Vimalraj.J
 * @Date 02-09-2017
 */
public interface ProjectDao {

   /**
    * insertProjectDetail() method inserts Project details 
    * in Project table.
    *
    * @param projectDetail - it contains Project details Stored in Project pojo
    *
    * @throws DaoException - It's a custom Exception ,occurs while Database 
    * Manipulation
    */
    void insertProjectDetails(Project projectDetail) throws DaoException;

   /**
   * getAllProjectDetails() gets all Project details from Database and 
   * stored list of Project pojo objects.
   *
   * @return - it returns list Of Project pojo Objects.
   *
   * @throws DaoException - It's a custom Exception ,occurs while Database 
   * Manipulation
   */  
    List<Project> getAllProjectDetails() throws DaoException;
   
   /** 
    * updateProjectDetails method updates the project details using Project 
    * model object.
    *
    * @param project model obejct  contains Project details
    *
    * @throws DaoException - It's a custom Exception ,occurs while Database 
    * Manipulation
    */
    void updateProjectDetails(Project project) throws DaoException;
    
   /**
    * removeProject() method deletes project as soft delete using projectCode
    *
    * @param projectCode - unique Code to identify particular Project
    *
    * @throws DaoException - It's a custom Exception ,occurs while Database 
    * Manipulation
    */   
    void removeProject(String projectCode) throws DaoException;
    
   /**
    * getProjectClientById method gets Project Pojo object of particular 
    * projectCode.
    *
    * @param projectCode - unique Id to refer Project
    *
    * @throws DaoException - It's a custom Exception ,occurs while Database 
    * Manipulation
    *
    * @return Project - Project model Object Contains Project details of given
    * project
    */  
    Project getProjectClientById(String projectCode) throws DaoException;
    
    /**
    * getProjectModelById method gets Project Pojo object of particular 
    * projectCode.
    *
    * @param projectCode - unique Id to refer Project
    *
    * @throws DaoException - It's a custom Exception ,occurs while Database 
    * Manipulation
    *
    * @return Project - Project model Object Contains Project details of given
    * project
    */  
    Project getProjectModelById(String projectCode) throws DaoException;
    
    /**
    * getProjectEmployeesById method gets Project Pojo object of particular projectCode.
    * @param projectCode - unique Id to refer Project
    *
    * @throws DaoException - it's acustom Exception, occurs while Database 
    * Manipulation
    *
    * @return Project - Project model Object Contains Project details
    */   
    Project getProjectEmployeesById(String projectCode) throws DaoException;
    
}
