package com.ideas2it.project.Dao.impl;

import java.lang.Boolean;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.FetchMode;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.Query;

import com.ideas2it.common.ConnectionFactory;
import com.ideas2it.common.Constants;
import com.ideas2it.employee.model.Employee;
import com.ideas2it.exception.DaoException;
import com.ideas2it.logger.ApplicationLogger;
import com.ideas2it.project.Dao.ProjectDao;
import com.ideas2it.project.model.Project;
import hibernate.sessionfactory.CreateSessionFactory;

/**
 * ProjectDaoImpl class implements ProjectDao methods provides necessary crud
 * operations .
 * @author VimalRaj J
 * @Date 02-09-2017
 */
public class ProjectDaoImpl implements ProjectDao {
    private SessionFactory sessionFactory = CreateSessionFactory.getInstance();
    static {
        ApplicationLogger.setLoggerInstance();
    }
    
   /**
    * @see com.ideas2it.project.Dao.ProjectDao 
    * #method insertProject()
    */ 
    public void insertProjectDetails(Project projectDetail) 
                                                          throws DaoException {
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            int id = (int) session.save(projectDetail);
            String projectCode = Constants.PROJECT_CODE_PREFIX + 
                                     String.format(Constants.NUMBER_FORMAT, id);
            projectDetail.setProjectCode(projectCode);
            session.update(projectDetail);                    
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }
            ApplicationLogger.error("Error Occured while Inserting Project "+
                              "details whose Client to be : "+ 
                              projectDetail.getClient().getClientCode() , ex);
			throw new DaoException("Something went Wrong ...Please try again "+
                                   "Later...");
        } finally {
            session.close();
        }
    }
   
    
   /**
    * @see com.ideas2it.project.Dao.ProjectDao 
    * #method getAllProjectDetails()
    */    
    public List<Project> getAllProjectDetails() throws DaoException {
        Session session = null;
        Transaction tx = null;
        List<Project> projects = null;
        try {
            session = sessionFactory.openSession();
            Criteria criteria = session.createCriteria(Project.class);
            criteria.add(Restrictions.eq("isActive",Boolean.TRUE));
            criteria.setFetchMode("client", FetchMode.JOIN);
            projects = criteria.list();
        } catch (HibernateException ex) {
            ApplicationLogger.error("Error Occured while get All Project "+
                                                                "Details.", ex);
			throw new DaoException("Something went Wrong ...Please try again "+
                                   "Later...");
        } finally {
            session.close();
        }
        return projects;      
    }
    
   /**
    * @see com.ideas2it.project.Dao.ProjectDao 
    * #method updateProjectDetails()
    */
    public void updateProjectDetails(Project projectDetail) 
                                                           throws DaoException {
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            session.update(projectDetail);
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }
            ApplicationLogger.error("Error Occured while Updating Project "+
                                    "Details of projectCode : " + 
                                     projectDetail.getProjectCode(), ex);
			throw new DaoException("Something went Wrong ...Please try again "+
                                   "Later...");
        } finally {
            session.close();
        }
    }

   /**
    * @see com.ideas2it.project.Dao.ProjectDao 
    * #method removeProject()
    */    
    public void removeProject(String projectCode) throws DaoException {
        Session session = null;
        Transaction tx = null;
        try {
            Project project = this.getProjectEmployeesById(projectCode);
            Set<Employee> employees = project.getEmployees();
            employees.clear();
            project.setIsActive(Boolean.FALSE);
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            session.update(project);
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }
            ApplicationLogger.error("Error Occured while Removing Project "+
                                   "Details of projectCode : "+ projectCode , ex);
			throw new DaoException("Something went Wrong ...Please try again "+
                                   "Later..."); 
        } finally {
            session.close();
        }
     }
      
   /**
    * @see com.ideas2it.project.Dao.ProjectDao 
    * #method getProjectModel()
    */   
    public Project getProjectClientById(String projectCode) throws DaoException {
        Session session = null;
        Transaction tx = null;
        Project project = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            Criteria criteria = session.createCriteria(Project.class);
            criteria.setFetchMode("client",FetchMode.JOIN);
            Criterion id = Restrictions.eq("projectCode",projectCode);
            Criterion isActive = Restrictions.eq("isActive",Boolean.TRUE);
            criteria.add(Restrictions.and(id,isActive));
            List<Project> results = criteria.list();
            if(!results.isEmpty()){
                project = results.get(0);
            } 
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }
            ApplicationLogger.error("Error occured while Checking Any Active "+
                                                 "Project is Available..", ex);
			throw new DaoException("Something went Wrong ...Please try again "+
                                   "Later...");
        } finally {
            session.close();
        }
        return project;
    }
    
    /**
    * @see com.ideas2it.project.Dao.ProjectDao 
    * #method getProjectModelById()
    */  
    public Project getProjectModelById(String projectCode) throws DaoException {
        Session session = null;
        Transaction tx = null;
        Project project = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            Criteria criteria = session.createCriteria(Project.class);
            criteria.setFetchMode("client",FetchMode.JOIN);
            criteria.setFetchMode("employees",FetchMode.JOIN);
            Criterion id = Restrictions.eq("projectCode",projectCode);
            Criterion isActive = Restrictions.eq("isActive",Boolean.TRUE);
            criteria.add(Restrictions.and(id,isActive));
            List<Project> results = criteria.list();
            if(!results.isEmpty()){
                project = results.get(0);
            } 
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }
            ApplicationLogger.error("Error occured while Checking Any Active "+
                                                 "Project is Available..", ex);
			throw new DaoException("Something went Wrong ...Please try again "+
                                   "Later...");
        } finally {
            session.close();
        }
        return project;
    }
    
   /**
    * @see com.ideas2it.project.Dao.ProjectDao 
    * #method getProjectEmployeesById()
    */   
    public Project getProjectEmployeesById(String projectCode) throws DaoException {
        Session session = null;
        Transaction tx = null;
        Project project = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            Criteria criteria = session.createCriteria(Project.class);
            criteria.setFetchMode("employees",FetchMode.JOIN);
            criteria.setFetchMode("employees.address",FetchMode.JOIN);
            Criterion id = Restrictions.eq("projectCode",projectCode);
            Criterion isActive = Restrictions.eq("isActive",Boolean.TRUE);
            criteria.add(Restrictions.and(id,isActive));
            List<Project> results = criteria.list();
            if(!results.isEmpty()){
                project = results.get(0);
            } 
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }
            ApplicationLogger.error("Error occured while Checking Any Active "+
                                                 "Project is Available..", ex);
			throw new DaoException("Something went Wrong ...Please try again "+
                                   "Later...");
        } finally {
            session.close();
        }
        return project;
    }
}


