package com.ideas2it.project.model;

import java.util.Set;

import com.ideas2it.employee.model.Employee;
import com.ideas2it.client.model.Client;

/**
 * Project pojo class provides setters/getters of following fields namely, 
 * project code ,title,description,domain. 
 * This class also overrides (toString() method of Object class.
 * @Author Vimalraj J
 * @Date 28/08/2017
 */
public class Project {    
    //private String clientId;
    private int id;
    private String projectCode;
    private String title;
    private String description;
    private String domain;
    private boolean isActive;
    private Client client;
    private Set<Employee> employees;
    
    public Project() {
    
    }
	
    public Project(Client client, String title, String description, 
                                              String domain ,boolean isActive) {
        this.client = client;
        this.title = title;
        this.description = description;
        this.domain = domain;
        this.isActive = isActive;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public int getId() {
        return this.id;
    }
    
    public void setClient(Client client) {
        this.client = client;
    }
    
    public Client getClient() {
        return this.client;
    }
    
    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }
    
    public boolean getIsActive() {
        return this.isActive;
    }
    
    /*public void setclientId(String clientId) {
        this.clientId = clientId;
    }
    
    public String getclientId() {
        return this.clientId;
    }
    */
    
    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description ) {
        this.description = description;
    } 

    public void setDomain(String domain) {
        this.domain = domain;
    }
    
    public void setEmployees(Set<Employee> employees) {
        this.employees = employees;
    }
    
    public Set<Employee> getEmployees() {
        return this.employees;
    }

    
    
    public String getProjectCode() {
        return this.projectCode;
    }
    
    public String getTitle() {
        return this.title;
    }
    public String getDescription() {
        return this.description;
    }

    public String getDomain() {
        return this.domain;
    }

   /**
    * toString method returns all the employee details in String type
    * @return returns string type of employee details.
    */
    public String toString() {
        String str = this.client.getClientCode() + "\t" + this.projectCode + "\t" + 
                     this.title +  "\t" + this.description +"\t"+  this.domain;    
        return str;        
    }
}
