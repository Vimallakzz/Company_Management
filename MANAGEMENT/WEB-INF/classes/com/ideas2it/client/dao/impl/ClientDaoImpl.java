package com.ideas2it.client.dao.impl;

import java.lang.Boolean;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.FetchMode;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.Query;
import org.json.simple.JSONObject;

import com.ideas2it.client.dao.ClientDao;
import com.ideas2it.client.model.Client;
import com.ideas2it.common.ConnectionFactory;
import com.ideas2it.common.Constants;
import com.ideas2it.exception.DaoException;
import com.ideas2it.logger.ApplicationLogger;
import com.ideas2it.project.model.Project;
import hibernate.sessionfactory.CreateSessionFactory;

/**
 * ClientDaoImpl class implements ClientDao methods provides necessary crud
 * operations .
 * @author VimalRaj J
 * @Date 01-09-2017
 */
public class ClientDaoImpl implements ClientDao {
    private SessionFactory sessionFactory = CreateSessionFactory.getInstance();
    static {
        ApplicationLogger.setLoggerInstance();
    }
    
   /**
    * @see com.ideas2it.client.Dao.ClientDao 
    * #method insertClientDetails()
    */
    @SuppressWarnings({"deprecation","unchecked"})
    public Client insertClientDetails(Client clientDetail) throws DaoException { 
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            int id = (int) session.save(clientDetail);
            String clientCode = Constants.CLIENT_CODE_PREFIX + 
                                     String.format(Constants.NUMBER_FORMAT, id);
            clientDetail.setClientCode(clientCode);
            session.update(clientDetail);
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }
            ApplicationLogger.error("Error Occured while Inserting Client "+
                 "details whose Email to be : "+ clientDetail.getEmailId(), ex);
            throw new DaoException("Something went Wrong ...Please try again "+
                                   "Later...");
        } finally {
            session.close();
        }
        return clientDetail;
    }
    
   /**
    * @see com.ideas2it.client.Dao.ClientDao 
    * #method validateEmailIdPhoneNo()
    */ 
    @SuppressWarnings("unchecked")
    public JSONObject validateEmailIdPhoneNo(String emailId,String phoneNo)
                                                           throws DaoException {
        Connection connection = null;
        PreparedStatement prepStatement = null;
        JSONObject errorFields = null;
        ResultSet resultSet = null;
        String query = "select EmailId,PhoneNo from Client where EmailId = ?"+
                                                            "or PhoneNo = ?";
        try {
            connection = ConnectionFactory.getConnection();
            prepStatement = connection.prepareStatement(query);
            prepStatement.setString(1,emailId);
            prepStatement.setString(2,phoneNo);
            resultSet = prepStatement.executeQuery();
            while (resultSet.next()) {
                if (resultSet.getString(1).equals(emailId)){
                    if (errorFields == null) {
                        errorFields = new JSONObject();
                    }
                    errorFields.put("emailId","EmailId Already Exists");
                }
                if (resultSet.getString(2).equals(phoneNo)){
                    if (errorFields == null) {
                        errorFields = new JSONObject();
                    }
                    errorFields.put("phoneNo","MobileNo Already Exists");
                }  
            }          
        } catch (SQLException ex) {
            throw new DaoException("Something went Wrong ...Please try again "+
                                   "Later...");
        } finally {
            ConnectionFactory.closeConnection(connection,prepStatement);
        } 
        return errorFields;
    }
    
   /**
    * @see com.ideas2it.client.Dao.ClientDao 
    * #method getAllClientDetails()
    */
    public List<Client> getAllClientDetails() throws DaoException {      
        Session session = null;
        Transaction tx = null;
        List<Client> clients = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            Criteria criteria = session.createCriteria(Client.class);
            criteria.add(Restrictions.eq("isActive",Boolean.TRUE));
            clients = criteria.list();
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }
            ApplicationLogger.error("Error Occured while get All Client "+
                                                                "Details", ex);
            throw new DaoException("Something went Wrong ...Please try again "+
                                   "Later...");
        } finally {
            session.close();
        }
        return clients;
    }

    /**
    * @see com.ideas2it.client.Dao.ClientDao 
    * #method updateClientDetails()
    */
    public void updateClientDetails(Client clientDetail) throws DaoException {
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            session.update(clientDetail);
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }
            ApplicationLogger.error("Error Occured while Updating Client "+
                                    "Details of ClientId : "+ 
                                    clientDetail.getClientCode(), ex);
            throw new DaoException("Something went Wrong ...Please try again "+
                                   "Later...");
        } finally {
            session.close();
        }
    }
    
   /**
    * @see com.ideas2it.client.Dao.ClientDao 
    * #method removeClient()
    */
    public void removeClient(String clientCode) throws DaoException {       
        Session session = null;
        Transaction tx = null;
        try {
            Client client = this.getClientProjectsById(clientCode);
            Set<Project> projects = client.getProjects();
            for(Project project : projects) {
                project.setIsActive(Boolean.FALSE);
                project.getEmployees().clear();
            }
            client.setIsActive(Boolean.FALSE);
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            session.update(client);                                         
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }
            ApplicationLogger.error("Error Occured while Deleting Client "+
                                     "Details of ClientId : "+ clientCode , ex);
            throw new DaoException("Something went Wrong ...Please try again "+
                                   "Later...");
        } finally {
            session.close();
        }

    }
   
   /**
    * @see com.ideas2it.client.Dao.ClientDao 
    * #method getClientModelById()
    */ 
    public Client getClientModelById(String clientCode) throws DaoException {
        Session session = null;
        Transaction tx = null;
        Client client = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            Criteria criteria = session.createCriteria(Client.class);
            criteria.setFetchMode("addresses",FetchMode.JOIN);
            criteria.setFetchMode("projects",FetchMode.JOIN);
            criteria.createAlias("projects","project",Criteria.LEFT_JOIN);
            
            Conjunction multipleAndConditions = Restrictions.conjunction();
            multipleAndConditions.add(Restrictions.eq("clientCode",clientCode))
                                 .add(Restrictions.eq("isActive",Boolean.TRUE));
            
            Disjunction multipleOrConditions = Restrictions.disjunction();
            multipleOrConditions.add(Restrictions.eq("project.isActive",Boolean.TRUE))
                                .add(Restrictions.isNull("project.isActive"));                                
            
            criteria.add(Restrictions.and(multipleAndConditions,multipleOrConditions));                          
            List<Client> results = criteria.list();
            if(!results.isEmpty()){
                client = results.get(0);
            }               
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }
            ApplicationLogger.error("Error Occure while getting ClientDetails "+
                                  "Of Particular ClientId : "+ clientCode, ex);
            throw new DaoException("Something went Wrong ...Please try again "+
                                   "Later...");  
        } finally {
            session.close();
        }
        return client;
    }
    
   /**
    * @see com.ideas2it.client.Dao.ClientDao 
    * #method getClientModelByCode()
    */ 
    public Client getClientAndAddressById(String clientCode) throws DaoException {
        Session session = null;
        Transaction tx = null;
        Client client = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            Criteria criteria = session.createCriteria(Client.class);
            criteria.setFetchMode("addresses",FetchMode.JOIN);
            Criterion id = Restrictions.eq("clientCode",clientCode);
            Criterion isActive = Restrictions.eq("isActive",Boolean.TRUE);
            criteria.add(Restrictions.and(id,isActive));
            List<Client> results = criteria.list();
            if(!results.isEmpty()){
                client = results.get(0);
            }      
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }
            ApplicationLogger.error("Error Occure while getting ClientDetails "+
                                  "Of Particular ClientId : "+ clientCode, ex);
            throw new DaoException("Something went Wrong ...Please try again "+
                                   "Later...");  
        } finally {
            session.close();
        }
        return client;
    }
    
   /**
    * getClientProjectsById method gets client Model object of particular clientId.
    * @param clientId - unique Id to refer Client
    *
    * @throws DaoException - custom Exception , it occurs while Database 
    * Manipulation
    *
    * @return client - Client model Object Contains Client details
    * Project details of Particular Client
    */
    private Client getClientProjectsById(String clientCode) throws DaoException {
        Session session = null;
        Transaction tx = null;
        Client client = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            Criteria criteria = session.createCriteria(Client.class);
            criteria.setFetchMode("projects",FetchMode.JOIN);
            criteria.setFetchMode("projects.employees",FetchMode.JOIN);
            criteria.createAlias("projects","project",Criteria.LEFT_JOIN);
            
            Conjunction multipleAndConditions = Restrictions.conjunction();
            multipleAndConditions.add(Restrictions.eq("clientCode",clientCode))
                                 .add(Restrictions.eq("isActive",Boolean.TRUE));
            
            Disjunction multipleOrConditions = Restrictions.disjunction();
            multipleOrConditions.add(Restrictions.eq("project.isActive",Boolean.TRUE))
                                .add(Restrictions.isNull("project.isActive"));
                             
            criteria.add(Restrictions.and(multipleAndConditions,multipleOrConditions));
            List<Client> results = criteria.list();
            if(!results.isEmpty()){
                client = results.get(0);
            }                  
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }
            ApplicationLogger.error("Error Occure while getting ClientDetails "+
                                  "Of Particular ClientId : "+ clientCode, ex);
            throw new DaoException("Something went Wrong ...Please try again "+
                                   "Later...");  
        } finally {
            session.close();
        }
        return client;
    }  
}
