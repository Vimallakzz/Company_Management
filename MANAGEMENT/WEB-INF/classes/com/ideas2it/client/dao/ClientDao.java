package com.ideas2it.client.dao;

import java.util.List;

import org.json.simple.JSONObject;

import com.ideas2it.client.model.Client;
import com.ideas2it.exception.DaoException;

/*
 * ClientDao interface provides database crud operations with some necessary 
 * methods 
 * @author Vimalraj.J
 * @Date 01-09-2017
 */
public interface ClientDao {

   /**
    * insertClientDetail() method inserts Client(Personal details) 
    * in Client table.
    *
    * @param clientDetail - contains Client Personal details in Client pojo
    *
    * @throws DaoException - It's a custom Exception ,occurs while Database 
    * Manipulation  
    */
    Client insertClientDetails(Client clientDetail) throws DaoException;
    
   /**
    * getAllClientDetailsAsList() gets all Client details from Database and 
    * stored list of Client pojo objects.
    *
    * @throws DaoException - It's a custom Exception, occurs while Database 
    * Manipulation
    *
    * @return - it returns list Of Client pojo Objects.
    */
    List<Client> getAllClientDetails() throws DaoException;
   
   /** 
    * updateClientDetails method updates the client details using Client 
    * model object.
    *
    * @param clientmodel obejct  contains Client personal details
    *
    * @throws DaoException - It's a custom Exception ,occurs while Database 
    * Manipulation
    */
    void updateClientDetails(Client client) throws DaoException;
    
   /**
    * removeClient() method deletes client as soft delete using clientcode get
    * from Client pojo Object
    *
    * @param clientCode - Unique Code to identify particular Client
    *
    * @throws DaoException - It's a custom Exception ,occurs while Database 
    * Manipulation
    */
    void removeClient(String clientCode) throws DaoException;
    
   /**
    * validateEmailIdPhoneNo() method validates EmailId or phoneNo already 
    * Exists or not
    *
    * @param emailId represents Client EmailId
    * @param phoneNo represents Client Contact No
    *
    * @throws DaoException - It's a custom Exception ,occurs while Database 
    * Manipulation
    *
    * @return ErrorFields of type JsonObject
    */
    JSONObject validateEmailIdPhoneNo(String emailId,String phoneNo) 
                                                            throws DaoException;
                                                            
   /**
    * getClientModelById method gets client Model object of particular 
    * clientId.
    *
    * @param clientId - unique Id to represent particular Client
    *
    * @throws DaoException - It's a custom Exception ,occurs while Database 
    * Manipulation
    *
    * @return client - Client model Object Contains Client details/
    * address details of Particular customer
    */
    Client getClientModelById(String clientCode) throws DaoException;
    
    /**
    * getClientAndAddressById method gets client Model object of particular 
    * clientId.
    *
    * @param clientId - unique Id to represent particular Client
    *
    * @throws DaoException - It's a custom Exception ,occurs while Database 
    * Manipulation
    *
    * @return client - Client model Object Contains Client details/
    * address details of Particular customer
    */
    Client getClientAndAddressById(String clientCode) throws DaoException;
}
