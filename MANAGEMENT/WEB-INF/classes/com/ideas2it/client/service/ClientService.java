package com.ideas2it.client.service;

import java.util.List;

import org.json.simple.JSONObject;

import com.ideas2it.client.model.Client;
import com.ideas2it.exception.DaoException;
import com.ideas2it.exception.UserDataException;

/**
 * ClientService interface provides basic crud operations methods, which needs
 * needs to implement, while implement this interface.
 * @author Vimalraj J
 * @date 29/08/2017
 */
public interface ClientService {

   /**
    * AddClientDetails method get client details as personal detail ,
    * permanent address, current address details in JSON Object.This methods  
    * stores the personal details in Client pojo Object. and send over DAO 
    * Layer to store in database
    *
    * @param PersonalDetail - provides basic Client details like id,name,etc..
    * @param permanentAddress / currentAddress - provides Address details like
    * flat no,street,etc..
    *
    * @throws DaoException - custom Exception , it occurs while Database 
    * Manipulation
    */
	void addClientDetails(Client client) throws DaoException, UserDataException;
	                    
	 /**
    * getAllClientDetails method gets Client details from database as List of
    * Client Model . It stores the list of address to to corresponding 
    * Client pojo objects.
    *
    * @throws DaoException - custom Exception , it occurs while Database 
    * Manipulation
    *
    * @return - it returns all the Client details as list of Client 
    * Model objects.
    */
	List<Client> getAllClientDetails() throws DaoException;
                    
   /**
    * modifyClient method updates Client records(personal/Address details)
    * using Client model object. This Object contains Edited Personal/ Address
    * detail .
    * @param client model Object contains Client personal details/list of
    * Addresses for particular customer
    *
    * @throws DaoException - custom Exception , it occurs while Database 
    * Manipulation
    */
	void modifyClient(Client client) throws DaoException;

   /**
    * removeClientById remove a particular project details by his 'EmpId'
    * @param clientCode clientCode --- Unique Id to remove  particular client
    * details
    *
    * @throws DaoException - custom Exception , it occurs while Database 
    * Manipulation
    */
	void removeClientById(String clientCode) throws DaoException;
   
   /**
    * getClientModelById method gets client Model object of particular clientCode.
    * @param clientCode - unique Id to refer Client
    *
    * @throws DaoException - custom Exception , it occurs while Database 
    * Manipulation
    *
    * @return client - Client model Object Contains Client details/
    * address details of Particular Client
    */
    public Client getClientModelById(String clientCode) throws DaoException;
   
   /**
    * getClientAndAddressById method gets client Model object of particular clientCode.
    * @param clientCode - unique Id to refer Client
    *
    * @throws DaoException - custom Exception , it occurs while Database 
    * Manipulation
    *
    * @return client - Client model Object Contains Client details/
    * address details of Particular Client
    */
	Client getClientAndAddressById(String clientCode) throws DaoException;
}
