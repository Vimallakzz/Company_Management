package com.ideas2it.client.service.impl;

import java.lang.Boolean;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.simple.JSONObject;

import com.ideas2it.address.model.Address;
import com.ideas2it.client.dao.ClientDao;
import com.ideas2it.client.dao.impl.ClientDaoImpl;
import com.ideas2it.client.model.Client;
import com.ideas2it.client.service.ClientService;
import com.ideas2it.common.Constants;
import com.ideas2it.exception.DaoException;
import com.ideas2it.exception.UserDataException;

/**
 * ClientServiceImpl class implementing ClientService interface.It serves  
 * crud operations(create/read/update/delete) on Client details.
 * This class stores the list of Client details in POJO objects and it stored 
 * in collection objects to retrive those information.
 * @Author Vimalraj.J
 * @Date 29/08/2017
 */
public class ClientServiceImpl implements ClientService {
    private ClientDao clientDao = new ClientDaoImpl();
    
   /**
    * @see com.ideas2it.client.service.ClientService 
    * #method addClientDetails()
    */
    public void addClientDetails(Client client) throws DaoException, UserDataException {							
        String emailId =  client.getEmailId();
        String phoneNo =  client.getPhoneNo();
        JSONObject errorFields = clientDao.
                                        validateEmailIdPhoneNo(emailId,phoneNo);
        if(errorFields == null) {
            clientDao.insertClientDetails(client);
        } else {
            throw new UserDataException("Failed to add Employee Details due to:",
                                                                   errorFields);
        }
    }

    /**
    * @see com.ideas2it.client.service.ClientService 
    * #method displayClientDetails()
    */
    @SuppressWarnings("unchecked")
    public  List<Client> getAllClientDetails() throws DaoException {
        List<Client> clients = clientDao.getAllClientDetails();
        return clients;
    }
    
   /**
    * @see com.ideas2it.client.service.ClientService 
    * #method modifyClient(String,,JSONObject)
    */
    @SuppressWarnings("unchecked")
    public void modifyClient(Client client) throws DaoException {       
        clientDao.updateClientDetails(client);
    }
    
   /**
    * @see com.ideas2it.client.service.ClientService 
    * #method removeClientDetails()
    */
    @SuppressWarnings("unchecked")
    public  void removeClientById(String clientCode) throws DaoException {
        clientDao.removeClient(clientCode);
    }
   
   /**
    * @see com.ideas2it.client.service.ClientService 
    * #method getClientModelById(String)
    */
    public Client getClientModelById(String clientCode) throws DaoException {
        return clientDao.getClientModelById(clientCode);
    }
    
   /**
    * @see com.ideas2it.client.service.ClientService 
    * #method getClientAndAddressById(String)
    */
    public Client getClientAndAddressById(String clientCode) throws DaoException {
        Client client = clientDao.getClientAndAddressById(clientCode);
        return client;
	} 
}
