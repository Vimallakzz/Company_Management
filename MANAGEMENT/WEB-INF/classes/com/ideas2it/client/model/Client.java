package com.ideas2it.client.model;

import java.util.Set;

import com.ideas2it.address.model.Address;
import com.ideas2it.project.model.Project;

/**
 * Client pojo class provides getters and Setters for basic client details 
 * like name,ContactNo,EmailId, Address Details.
 * This class also overrides (toString() method of Object class.
 * @Author Vimalraj J
 * @Date 28/08/2017
 */
public class Client {
    int id;
    private String clientCode;
    private String name;
    private String emailId;
    private String phoneNo;
    private boolean isActive;
    private Set<Address> addresses;
    private Set<Project> projects;
    
    public Client() {
    
    }
    
    public Client(String name, String emailId, String phoneNo, boolean isActive) {
        this.name = name;
        this.emailId = emailId;
        this.phoneNo = phoneNo;
        this.isActive = isActive;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }
    
    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }
    
    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }
    
    public boolean getIsActive() {
        return this.isActive;
    }
    
    public int getId() {
        return this.id;
    }
    
    public String getName(){
        return this.name;
    }
    
    public String getEmailId() {
        return this.emailId;
    }
    
    public String getPhoneNo() {
        return this.phoneNo;
    }
    
    public String getClientCode() {
        return this.clientCode;
    }
    
    public void setAddresses(Set<Address> addresses) {
        this.addresses = addresses;
    }
    
    public void setProjects(Set<Project> projects) {
        this.projects = projects;
    }
    
    public Set<Project> getProjects() {
        return this.projects;
    }
    
    public Set<Address> getAddresses() {
        return this.addresses;
    }
    
   /**
    * toString method returns all the employee details in String type
    * @return returns string type of employee details.
    */
    public String toString() {
        String str ="ClientCode : " + this.clientCode + "\nName : " + this.name + 
                "\nEmailId :" + this.emailId + "\nphoneNo : " + this.phoneNo;   
    return str;        
    }
}
