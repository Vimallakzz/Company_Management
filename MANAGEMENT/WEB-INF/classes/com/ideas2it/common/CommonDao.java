package com.ideas2it.common;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

import org.hibernate.cfg.Configuration;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.SQLQuery;
import org.hibernate.Transaction;
import org.hibernate.Query;

import hibernate.sessionfactory.CreateSessionFactory;

public class CommonDao {

    public static int  getNextAutoIncrementValue(String tableName) {
        SessionFactory sessionFactory = CreateSessionFactory.getInstance();
        //System.out.println("Object : " + sessionFactory);
        int id = -1;
        try {
            Session session = sessionFactory.openSession();
            Transaction tx = session.beginTransaction();
            String sql = "SELECT AUTO_INCREMENT FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA =  'CompanyManagement' AND   TABLE_NAME   = :n1";           
            SQLQuery query = session.createSQLQuery(sql);
            query.setParameter("n1",tableName);
            List results = query.list();
            id = ((BigInteger)results.get(0)).intValue();
            session.close();
        } catch (HibernateException ex) {
        
            ex.printStackTrace();
        }
        return id;
    }
}
