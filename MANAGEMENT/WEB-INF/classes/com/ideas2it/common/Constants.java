package com.ideas2it.common;

/**
 * Constants class provides Constant fiels require for Application
 * @Author Vimalraj.J
 * @DATE 28/08/2017
 */
public class Constants {

	public static final String EMPTY_STRING = "";
	public static final String DATE_FORMAT = "dd/MM/yyyy";
	public static final String YEARS = "Years";
	public static final String WEEKS = "Weeks";
	public static final String DAYS = "Days";
	public static final String COMMA_SEPERATOR =  " , ";

	// Employee  validation regex
	public static final String EMP_ID_PATTERN = "[0-9]{4,6}";
	public static final String EMP_NAME_PATTERN = "[A-Z a-z.]{5,15}";
	public static final String EMAILID_PATTERN = "[A-Za-z0-9]+[_A-Za-z0-9-+]*"+
                           "@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,5})";
	public static final String MOBILE_NO_PATTERN = "[987][0-9]{9}";
	public static final String DATE_PATTERN ="[123][0-9]/[01][1-9]/[0-9]{4}";

    //Project Validation Regex
    public static final String PROJ_CODE_PATTERN = "[0-9]{4,6}";
	public static final String PROJ_NAME_PATTERN = "[A-Za-z0-9 .]{5,15}";
    public static final String DESCRIPTION_PATTERN = "[a-zA-Z0-9 -.,/\\[\\]]"+
                                                                      "{5,255}";
	public static final String DOMAIN_PATTERN = "[a-zA-Z0-9.,/]{5,50}";
	
	public static final String EMPLOYEE_CODE_PREFIX = "I2I-E";
	public static final String PROJECT_CODE_PREFIX = "I2I-P";
	public static final String CLIENT_CODE_PREFIX = "I2I-C";
	public static final String NUMBER_FORMAT = "%04d";

}
