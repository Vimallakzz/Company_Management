package com.ideas2it.common;

/**
 * ErrorCodes provides error messages for project 
 * @author Vimalraj. j
 * @date 29/08/2017
 */
public class ErrorCodes {
   //--------Employee details error messages-------
    public static final String EMP_ID = "Should contains only digits... "+
                                                  "Not more than 4-6 digits.";
    public static final String EMP_NAME = "Name should contains only alphabets"+
                                            "...Not more than 5-15 characters";
    public static final String EMAIL_ID = "Invalid Email Id";
    public static final String PHONE_NO = "Invalid Mobile No";
    public static final String DATE_PATTERN = "Date format should be "+
                                                                   "dd/MM/yyyy";
     
    //--------Project details error messages----------
    
    public static final String PROJECT_CODE = "Should contains only digits.."+
                                                    " Not more than 6 digits.";
    public static final String TITLE = "Name should contains only AlphaNumeric"+
                                               " Not more than 5-15 characters";
    public static final String DESCRIPTION = "Description should contains"+
                                                        " 5 to 255 characters ";
    public static final String DOMAIN ="Domain should contains"+
                                                        " 5 to 50 characters ";
}
