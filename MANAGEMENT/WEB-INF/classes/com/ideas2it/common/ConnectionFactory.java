package com.ideas2it.common;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * ConnectionFactory class estblish connection bw database and Java Application
 * It also used to close connection.
 * @author Vimalraj.j
 * @date 29/08/2017
 */
public class ConnectionFactory {
    private static String myDriver = "com.mysql.jdbc.Driver";
    private static String url = "jdbc:mysql://localhost:3306/CompanyManagement";
    private static String userName = "root";
    private static String password = "root";
    
   /**
    * getConnection methods creates connection bw java Application and Database
    * and returns the connection object.
    *
    * @returns it returns connection object to execute Database operations
    */
    public static Connection getConnection () {
        Connection connection = null;
        try {
            Class.forName(myDriver);
            connection = DriverManager.getConnection(url,userName,password);
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch(ClassNotFoundException e) {
            e.printStackTrace();
        }
        
      return connection;  
    }
   
   /**
    * closeConnection method closes the connection between Java application 
    * and Database
    *
    * @param connection - connection object,used to close the connection
    * @param prepStatement - PreparedStatement
    */
    public static void closeConnection(Connection connection , 
                                              PreparedStatement prepStatement) {
        try {
            if (connection != null) {
                connection.close();
            }
            if (prepStatement != null) {
                prepStatement.close();
            }
       }catch (SQLException ex) {
            ex.printStackTrace();
       } 
    }
}
