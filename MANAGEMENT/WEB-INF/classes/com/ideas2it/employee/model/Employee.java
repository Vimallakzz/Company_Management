package com.ideas2it.employee.model;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.text.ParseException;
import java.text.SimpleDateFormat;  

import com.ideas2it.address.model.Address;
import com.ideas2it.common.Constants;
import com.ideas2it.project.model.Project;
import com.ideas2it.util.DateUtil;

/**
 * Employee pojo class provides setters/getters of following fields namely, 
 * empid,empname,emailid,phoneno,dateofbirth,dateofjoining. 
 * This class also overrides (toString(),equals()) method of Object class.
 * @Author Vimalraj J
 * @Date 28/08/2017
 */
public class Employee{
    private int id;
    private String empCode;
    private String name;
    private String emailId;
    private String phoneNo;
    private Date dateOfBirth;
    private Date dateOfJoining;
    private boolean isActive;
    private Set<Address> addresses;
    private Set <Project> projects;
    
    public Employee() {
    
    }
    
    public Employee(String name,String emailId,String phoneNo,
                         Date dateOfBirth,Date dateOfJoining, boolean isActive) {        
        this.name = name;
        this.emailId = emailId;
        this.phoneNo = phoneNo;
        this.dateOfBirth = dateOfBirth;
        this.dateOfJoining = dateOfJoining;
        this.isActive = isActive;
    } 
    
    public void setId(int id) {
        this.id = id;
    }
    
    public void setEmpCode(String empCode) {
        this.empCode = empCode;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public void setDateOfJoining(Date dateOfJoining) {
        this.dateOfJoining = dateOfJoining;
    }
    
    public void setAddresses(Set<Address> addresses) {
        this.addresses = addresses;
    }
    
    public void setProjects(Set <Project> projects) {
        this.projects = projects;
    }
    
    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }
    
    public boolean getIsActive() {
        return this.isActive;
    }
    
    public int getId() {
        return this.id;
    }
    
    public Set<Address> getAddresses() {
        return this.addresses;
    }
    
    public Set <Project> getProjects() {
        return this.projects;
    }
    
    public String getEmpCode() {
        return this.empCode;
    }
    
    public String getName() {
        return this.name;
    }

    public String getPhoneNo() {
        return this.phoneNo;
    }
  
    public String getEmailId() {
        return this.emailId;
    }

    public Date getDateOfBirth() {
        return this.dateOfBirth;
    }

    public Date getDateOfJoining() {
        return this.dateOfJoining;
    }

   /**
    * getAge method return age of employee by using dateOfBirth variable
    * @return  Age of String type.  
    */
    public String getAge()  {
        String age = "";
        Date today = new Date();    
        int noOfDays = DateUtil.calculateDateDiffInDays(today, 
                                                              this.dateOfBirth);
        int years = noOfDays / 365 ;
        int weeks = (noOfDays % 365) /7 ;
        int days =  (noOfDays % 365) % 7 ;
        age = years + Constants.YEARS + Constants.COMMA_SEPERATOR + 
              weeks + Constants.WEEKS + Constants.COMMA_SEPERATOR + 
              days  + Constants.DAYS;              
        return age;       
    }

   /**
    * getExperience method return age of employe by using dateOfJoining variable
    * @return  Experience of String type.  
    */
    public String getExperience() {
        String experience = "";
        Date today= new Date();    
        int noOfDays = DateUtil.calculateDateDiffInDays(today, 
                                                            this.dateOfJoining);
        int years = noOfDays / 365 ;
        int weeks = (noOfDays % 365) /7 ;
        int days =  (noOfDays % 365) % 7 ;
        experience = years + Constants.YEARS + Constants.COMMA_SEPERATOR + 
                     weeks + Constants.WEEKS + Constants.COMMA_SEPERATOR + 
                     days  + Constants.DAYS;   
        return experience;               
    }
    
   /**
    * toString method returns all the employee details in String type
    * @return returns string type of employee details.
    */
    public String toString() {
        String str ="EmpCode : " + this.empCode + "\nName : " + this.name + 
                "\nEmailId :" + this.emailId + "\nphoneNo : " + this.phoneNo + 
                "\nDateOfBirth : " + this.dateOfBirth + 
                "\nDateOfJoining : " + this.dateOfJoining +
                "\nAge : "+this.getAge() + 
                "\nExperience : "+ this.getExperience();   
    return str;        
    }
}
