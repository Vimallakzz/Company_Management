package com.ideas2it.employee.Dao.impl;

import java.lang.Boolean;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.FetchMode;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.Query;
import org.json.simple.JSONObject;

import com.ideas2it.common.ConnectionFactory;
import com.ideas2it.common.Constants;
import com.ideas2it.employee.Dao.EmployeeDao;
import com.ideas2it.employee.model.Employee;
import com.ideas2it.exception.DaoException;
import com.ideas2it.logger.ApplicationLogger;
import com.ideas2it.project.model.Project;
import hibernate.sessionfactory.CreateSessionFactory;

/**
 * EmployeeDaoImpl class implements EmployeeDao methods provides necessary crud
 * operations .
 * @author VimalRaj J
 * @Date 01-09-2017
 */
public class EmployeeDaoImpl implements EmployeeDao {
    private SessionFactory sessionFactory = CreateSessionFactory.getInstance();
    
    static {
        ApplicationLogger.setLoggerInstance();
    }
    
   /**
    * @see com.ideas2it.employee.Dao.EmployeeDao 
    * #method insertEmployeeDetails()
    */
    @SuppressWarnings({"deprecation","unchecked"})
    public void insertEmployeeDetails(Employee employeeDetail) 
                                                          throws DaoException {
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            int id = (int) session.save(employeeDetail);
            String empCode = Constants.EMPLOYEE_CODE_PREFIX + 
                                     String.format(Constants.NUMBER_FORMAT, id);
            employeeDetail.setEmpCode(empCode);
            session.update(employeeDetail);                      
            tx.commit();

        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }
            ApplicationLogger.error("Error Occured while Inserting Empoyee "+
                 "details whose Email to be:" + employeeDetail.getEmailId(),ex);                      
	        throw new DaoException("Something went Wrong... Please try again "+
                                   "Later...");
        } finally {
            session.close();
        }
    }
    
    /**
    * @see com.ideas2it.employee.Dao.EmployeeDao 
    * #method getAllEmployeeDetails()
    */
    public List<Employee> getAllEmployeeDetails() throws DaoException {
        List<Employee> employees = null;
        Session session =null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            Criteria criteria = session.createCriteria(Employee.class);
            criteria.add(Restrictions.eq("isActive",Boolean.TRUE));
            employees = criteria.list();
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }
            ApplicationLogger.error("Error Occured while get All Employee "+
                                                                 "Details ",ex);                       
	        throw new DaoException("Something went Wrong... Please try again "+
                                   "Later...");
        } finally {
            session.close();
        }
        return employees; 
    }

    /**
    * @see com.ideas2it.employee.Dao.EmployeeDao 
    * #method updateEmployeeDetails()
    */
    public void updateEmployeeDetails(Employee employeeDetail)
                                                           throws DaoException {
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            session.update(employeeDetail);
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }
            ApplicationLogger.error("Error Occured while Updating Empoyee "+
                                    "details of EmployeeId code : " + 
                                    employeeDetail.getEmpCode(), ex);                       
	        throw new DaoException("Something went Wrong... Please try again "+
                                   "Later...");
        } finally {
            session.close();
        }
    }
    
   /**
    * @see com.ideas2it.employee.Dao.EmployeeDao 
    * #method removeEmployee()
    */
    public void removeEmployee(String empCode) throws DaoException {
        Session session =null;
        Transaction tx = null;
        try {
            Employee employee = this.getEmployeeProjectsById(empCode);
            Set<Project> projects = employee.getProjects();
            projects.clear();
            employee.setIsActive(Boolean.FALSE);
            session =  sessionFactory.openSession();
            tx = session.beginTransaction();
            session.update(employee);
            tx.commit(); 
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }
            ApplicationLogger.error("Error Occured while Deleting Employee "+
                                    "Details of Employee code : " + empCode, ex);                       
	        throw new DaoException("Something went Wrong... Please try again "+
                                   "Later...");
        } finally {
            session.close();
        }
     }

   /**
    * @see com.ideas2it.employee.Dao.EmployeeDao 
    * #method validateEmailIdPhoneNo()
    */ 
    @SuppressWarnings("unchecked")
    public JSONObject validateEmailIdPhoneNo(String emailId,String phoneNo) 
                                                          throws DaoException  {
        Connection connection = null;
        PreparedStatement prepStatement = null;
        JSONObject errorFields = null;
        ResultSet resultSet = null;
        String query = "select EmailId,PhoneNo from Employee where EmailId = ?"+
                                                               "or PhoneNo = ?";
        try {
            connection = ConnectionFactory.getConnection();
            prepStatement = connection.prepareStatement(query);
            prepStatement.setString(1,emailId);
            prepStatement.setString(2,phoneNo);
            resultSet = prepStatement.executeQuery();
            while (resultSet.next()) {
                if (resultSet.getString(1).equals(emailId)) {
                    if(errorFields == null) {
                        errorFields = new JSONObject();
                    }
                    errorFields.put("emailId","EmailId Already Exists");
                }
                if (resultSet.getString(2).equals(phoneNo)) {
                    if (errorFields == null) {
                        errorFields = new JSONObject();
                    }
                    errorFields.put("phoneNo","MobileNo Already Exists");
                }   
            }
        } catch (SQLException ex) {
			throw new DaoException("Something went Wrong ...Please try again "+
                                   "Later...");
        } finally {
            ConnectionFactory.closeConnection(connection,prepStatement);
        } 
        return errorFields;
    }
    
   /**
    * @see com.ideas2it.employee.Dao.EmployeeDao 
    * #method isEmployeeAvailable()
    */ 
    public boolean isEmployeeAvailable(int empId) throws DaoException {
        boolean status = Boolean.FALSE;
        Session session =null;
        Transaction tx = null;
        try {           
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            Employee employee = (Employee) session.get(Employee.class, empId);
            if (employee != null) {
                if(employee.getIsActive()) {
                    status = Boolean.TRUE;
                }
            }
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }
            ApplicationLogger.error("Error Occured while checking employee is "+
                              "Available or not.. Employee ID: "+ empId , ex);                       
	        throw new DaoException("Something went Wrong... Please try again "+
                                   "Later...");
        } finally {
            session.close();
        }
        return status;
    }
    
   /**
    * @see com.ideas2it.employee.Dao.EmployeeDao 
    * #method isEmpty()
    */ 
    public boolean isEmpty() throws DaoException {
        Session session =null;
        Transaction tx = null;
        Criteria criteria = null;
        boolean isEmpty = Boolean.TRUE;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            criteria = session.createCriteria(Employee.class);
            criteria.add(Restrictions.eq("isActive",Boolean.TRUE));
            List results = criteria.list();
            if(!results.isEmpty()) {
                isEmpty = Boolean.FALSE;
            }
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }
            ApplicationLogger.error("Error occured while Checking Any Active "+
                                              "employee is Available...." , ex);                       
	        throw new DaoException("Something went Wrong... Please try again "+
                                   "Later...");
        } finally {
            session.close();
        }
        return isEmpty;
    }
    
   /**
    * @see com.ideas2it.employee.Dao.EmployeeDao 
    * #method getEmployeeModel()
    */ 
    public Employee getEmployeeModelById(String empCode) throws DaoException {
        Employee employee = null;
        Session session =null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            Criteria criteria = session.createCriteria(Employee.class);
            criteria.setFetchMode("addresses", FetchMode.JOIN);
            criteria.setFetchMode("projects",FetchMode.JOIN);
            criteria.setFetchMode("projects.client",FetchMode.JOIN);
            Criterion id = Restrictions.eq("empCode",empCode);
            Criterion isActive = Restrictions.eq("isActive",Boolean.TRUE);
            criteria.add(Restrictions.and(id,isActive));
            List<Employee> results = criteria.list();
            if(!results.isEmpty()){
                employee = results.get(0);
            }
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }
            ApplicationLogger.error("Error Occure while getting Employee "+
                              "Details Of Particular EmployeeId :"+ empCode, ex);                       
	        throw new DaoException("Something went Wrong... Please try again "+
                                   "Later...");
        } finally {
            session.close();
        }
        return employee;
    }
    
    /**
    * @see com.ideas2it.employee.Dao.EmployeeDao 
    * #method getEmployeeAndAddressById()
    */ 
    public Employee getEmployeeAndAddressById(String empCode) throws DaoException {
        Employee employee = null;
        Session session =null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            Criteria criteria = session.createCriteria(Employee.class);
            criteria.setFetchMode("addresses",FetchMode.JOIN);
            Criterion id = Restrictions.eq("empCode",empCode);
            Criterion isActive = Restrictions.eq("isActive",Boolean.TRUE);
            criteria.add(Restrictions.and(id,isActive));
            List<Employee> results = criteria.list();
            if(!results.isEmpty()){
                employee = results.get(0);
            }
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }
            ApplicationLogger.error("Error Occure while getting Employee "+
                              "Details Of Particular EmployeeId :"+ empCode, ex);                       
	        throw new DaoException("Something went Wrong... Please try again "+
                                   "Later...");
        } finally {
            session.close();
        }
        return employee;
    }
    
   /**
    * @see com.ideas2it.employee.Dao.EmployeeDao 
    * #method getEmployeeProjectsById()
    */ 
    public Employee getEmployeeProjectsById(String empCode) throws DaoException {
        Employee employee = null;
        Session session =null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            Criteria criteria = session.createCriteria(Employee.class);
            criteria.setFetchMode("projects",FetchMode.JOIN);
            criteria.setFetchMode("projects.client",FetchMode.JOIN);
            Criterion id = Restrictions.eq("empCode",empCode);
            Criterion isActive = Restrictions.eq("isActive",Boolean.TRUE);
            criteria.add(Restrictions.and(id,isActive));
            List<Employee> results = criteria.list();
            if(!results.isEmpty()){
                employee = results.get(0);
            }
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }
            ApplicationLogger.error("Error Occure while getting Employee "+
                              "Details Of Particular EmployeeId :"+ empCode, ex);                       
	        throw new DaoException("Something went Wrong... Please try again "+
                                   "Later...");
        } finally {
            session.close();
        }
        return employee;
    }
} 

