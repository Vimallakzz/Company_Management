package com.ideas2it.employee.Dao;

import java.util.List;

import org.json.simple.JSONObject;

import com.ideas2it.employee.model.Employee;
import com.ideas2it.exception.DaoException;

/*
 * EmployeeDao interface provides database crud operations with some necessary 
 * methods 
 * @author Vimalraj.J
 * @Date 01-09-2017
 */
public interface EmployeeDao {

   /**
    * insertEmployeeDetail() method inserts Employee(Personal details) 
    * in Employee table.
    *
    * @param employeeDetail -contains Employee Personal details in Employee pojo
    *
    * @throws DaoException - It's a custom Exception ,occurs while Database 
    * Manipulation 
    */
    void insertEmployeeDetails(Employee employeeDetail) throws DaoException;
    
   /**
    * getAllEmployeeDetailsAsList() gets all Employee details from Database and 
    * stored list of Employee pojo objects.
    *
    * @throws DaoException - It's a custom Exception ,occurs while Database 
    * Manipulation
    *
    * @return - it returns list Of Employee pojo Objects.
    */
    List<Employee> getAllEmployeeDetails() throws DaoException;
   
   /** 
    * updateEmployeeDetails method updates the employee details using Employee 
    * model object.
    *
    * @param employeemodel obejct  contains Employee personal details/Address 
    * Details
    *
    * @throws DaoException - It's a custom Exception ,occurs while Database 
    * Manipulation
    */
    void updateEmployeeDetails(Employee employee) throws DaoException;
    
   /**
    * removeEmployee() method deletes employee as soft delete using Empcode
    *
    * @param empCode - unique Code to find Particular Customer.
    *
    * @throws DaoException - It's a custom Exception ,occurs while Database 
    * Manipulation
    */
    void removeEmployee(String empCode) throws DaoException;
    
   /**
    * validateEmailIdPhoneNo() method validates EmailId or phoneNo already 
    * Exists or not
    *
    * @param emailId represents Employee EmailId
    * @param phoneNo represents Employee Contact No
    *
    * @throws DaoException - It's a custom Exception ,occurs while Database 
    * Manipulation
    *
    * @return it returns the error message in form of Json Object
    */
    JSONObject validateEmailIdPhoneNo(String emailId,String phoneNo) 
                                                            throws DaoException;
    
    /**
    * getEmployeeModelByCode method gets employee Model object of particular empCode.
    * @param empCode - unique Id to refer Employee
    *
    * @throws DaoException - It's a custom Exception ,occurs while Database 
    * Manipulation
    *
    * @return employee - Employee model Object Contains Employee details/
    * address details of Particular customer
    */
    Employee getEmployeeModelById(String empCode) throws DaoException;
    
   /**
    * getEmployeeAndAddressById method gets employee Model object of particular empCode.
    * @param empCode - unique Id to refer Employee
    *
    * @throws DaoException - It's a custom Exception ,occurs while Database 
    * Manipulation
    *
    * @return employee - Employee model Object Contains Employee details/
    * address details of Particular customer
    */
    Employee getEmployeeAndAddressById(String empCode) throws DaoException;
    
    /**
    * getEmployeeProjectsById method gets employee Model object of particular empCode.
    * @param empCode - unique Id to refer Employee
    *
    * @throws DaoException - It's a custom Exception ,occurs while Database 
    * Manipulation
    *
    * @return employee - Employee model Object Contains Employee details/
    * address details of Particular customer
    */
    public Employee getEmployeeProjectsById(String empCode) throws DaoException;
}
