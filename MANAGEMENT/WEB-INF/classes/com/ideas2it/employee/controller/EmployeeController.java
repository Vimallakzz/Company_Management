package com.ideas2it.employee.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import org.json.simple.JSONObject;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;

import com.ideas2it.address.model.Address;
import com.ideas2it.common.Constants;
import com.ideas2it.employee.model.Employee;
import com.ideas2it.employee.service.EmployeeService;
import com.ideas2it.employee.service.impl.EmployeeServiceImpl;
import com.ideas2it.exception.DaoException;
import com.ideas2it.exception.UserDataException;
import com.ideas2it.project.model.Project;
import com.ideas2it.project.service.ProjectService;
import com.ideas2it.project.service.impl.ProjectServiceImpl;
import com.ideas2it.util.ValidationUtil;

/**
 * EmployeeController class provides an option for 
 * Crud operations(read/update/delete/create) .
 * @Author Vimalraj 
 * @Date   29/08/2017 
 */
public class EmployeeController extends HttpServlet { 
    private EmployeeService employeeService = new EmployeeServiceImpl();
    
    public boolean isSessionExpired(HttpServletRequest req, 
                  HttpServletResponse res) throws ServletException,IOException {
        HttpSession session =  req.getSession(false);
        if(session != null && session.getAttribute("userName") != null) {
            return true;  
        }
        return false;
    }
    
    public void doGet(HttpServletRequest req,HttpServletResponse res)  
                                           throws ServletException,IOException {       
        boolean status = this.isSessionExpired(req,res);
        if(status) {
            String parameter = req.getParameter("parameter");
            if(parameter != null) {
                if (parameter.equals("addEmployeeDetails")) {
                    this.addEmployee(req,res);
                } else if (parameter.equals("getAllEmployees")) {
                    this.getAllEmployees(req,res);
                } else if (parameter.equals("getEmployee")) {
                    this.getEmployee(req,res);
                } else if (parameter.equals("updateEmployeeDetails")) {
                    this.updateEmployee(req,res);
                }else if (parameter.equals("deleteEmployee")) {
                    this.deleteEmployee(req,res);
                } else if (parameter.equals("getEmployeeProjects")) {
                    String empCode = req.getParameter("empId");
                    try {
                        ProjectService projectService = new ProjectServiceImpl();
                        Employee employee = employeeService.getEmployeeProjectsById(empCode);
                        List<Project> projects = projectService.getAllProjectDetails();
                        req.setAttribute("employee",employee);
                        req.setAttribute("projects",projects);
                        RequestDispatcher rd = req.getRequestDispatcher("/jsp/employee/assignprojects.jsp");
                        rd.forward(req,res);
                    } catch (DaoException ex) {
                        System.out.println(ex.getMessage());
                    }
                } else {
                    res.sendError(503);
                }
            } else {
                res.sendError(503);
            }
        } else {
            res.sendRedirect("/MANAGEMENT/html/login.html");
        }
    }
    
    public void doPost(HttpServletRequest req,HttpServletResponse res)  
                                           throws ServletException,IOException {
        boolean status = this.isSessionExpired(req,res);
        if(status) {
            String parameter = req.getParameter("parameter");
            if(parameter != null) {
                if (parameter.equals("addEmployeeDetails")) {
                    this.addEmployee(req,res);
                } else if (parameter.equals("getAllEmployees")) {
                    this.getAllEmployees(req,res);
                } else if (parameter.equals("getEmployee")) {
                    this.getEmployee(req,res);
                } else if (parameter.equals("updateEmployeeDetails")) {
                    this.updateEmployee(req,res);
                }else if (parameter.equals("deleteEmployee")) {
                    this.deleteEmployee(req,res);
                } else if (parameter.equals("getEmployeeProjects")) {
                    String empCode = req.getParameter("empId");
                    try {
                        ProjectService projectService = new ProjectServiceImpl();
                        Employee employee = employeeService.getEmployeeProjectsById(empCode);
                        List<Project> projects = projectService.getAllProjectDetails();
                        req.setAttribute("employee",employee);
                        req.setAttribute("projects",projects);
                        RequestDispatcher rd = req.getRequestDispatcher("/jsp/employee/assignprojects.jsp");
                        rd.forward(req,res);
                    } catch (DaoException ex) {
                        System.out.println(ex.getMessage());
                    }
                } else {
                    res.sendError(503);
                }
            } else {
                res.sendError(503);
            }
        } else {
            res.sendRedirect("/MANAGEMENT/html/login.html");
        }
    }

   /**
    * addEmployee method gets Personal Information / Address information from
    * User to add the Employee details in database
    */
    @SuppressWarnings("unchecked")
    public  void addEmployee(HttpServletRequest req,HttpServletResponse res)  
                                           throws ServletException,IOException {
        
        Employee employee = this.readPersonalDetailsFromRequest(req,res);
        Address address = this.getAddressDetailsFromRequest(req,res,true);
        Set<Address> addresses = new HashSet<Address>();
        addresses.add(address);
        String isSameAsPermanent = req.getParameter("isSameAsPermanent");
        if(isSameAsPermanent == null) {
            address = this.getAddressDetailsFromRequest(req,res,false);
            addresses.add(address);
        }
        employee.setAddresses(addresses);
        try {
            employeeService.addEmployeeDetails(employee);
            req.setAttribute("message","Employee Details Added successfully"); 
            this.getAllEmployees(req,res);
        } catch (DaoException ex) {
            System.out.println(ex.getMessage());
        } catch (UserDataException ex) {
            System.out.println(ex.getMessage() + " " + ex.getErroredUserData());
        }
    }
    
    /**
    * getEmployeeDetailsFromUser method gets employee details from user and 
    * stores the details in JSON object
    *
    * @return it returns Json object that contains personal details of 
    * Employee 
    */
    @SuppressWarnings("unchecked")
    public  Employee readPersonalDetailsFromRequest(HttpServletRequest req,
                                                      HttpServletResponse res) 
                                           throws ServletException,IOException {
                                           
        String name = req.getParameter("empName");
        String emailId = req.getParameter("emailId");
        String phoneNo = req.getParameter("phoneNo");
        String birthDate = req.getParameter("dateOfBirth"); 
        String joiningDate = req.getParameter("dateOfJoining");
        Date dateOfBirth = null ;
        Date dateOfJoining = null;
        try {
            dateOfBirth = new SimpleDateFormat("yyyy-MM-dd").parse(birthDate);  
            dateOfJoining = new SimpleDateFormat("yyyy-MM-dd").
                                                            parse(joiningDate);          
        } catch (ParseException ex) {
            
        }
        Employee employee = new Employee(name, emailId, phoneNo, dateOfBirth,
                                                   dateOfJoining, Boolean.TRUE);
        return employee; 
    }
    
   /**
    * getAddressDetailsFromRequest method gets Address Detail from User and returns
    * Address Details in form of Json
    *
    * @return it returns address detail in key value pair stored in JSONObject
    */ 
    public Address getAddressDetailsFromRequest(HttpServletRequest req, 
                                                   HttpServletResponse res, boolean status)  
                                           throws ServletException,IOException {
        Address address = null;                                  
        // if status is false then read temporary address details otherwise read permanent addressDetails
        if(!status) {
            String flatNo = req.getParameter("t_flatNo");
            String streetName = req.getParameter("t_streetName");
            String city = req.getParameter("t_city");
            String state = req.getParameter("t_state");
            String zipCode = req.getParameter("t_zipCode");
            address = new Address(flatNo, streetName, city, state, zipCode);
            address.setIsPermanent(Boolean.FALSE);
            
        } else {
            String flatNo = req.getParameter("p_flatNo");
            String streetName = req.getParameter("p_streetName");
            String city = req.getParameter("p_city");
            String state = req.getParameter("p_state");
            String zipCode = req.getParameter("p_zipCode");
            address = new Address(flatNo, streetName, city, state, zipCode);
            address.setIsPermanent(Boolean.TRUE);

        }                               
        return address;
    }

   /**
    * displayEmployee method display all employee Personal Details and Address
    * Details .
    */
    public void getAllEmployees(HttpServletRequest req,HttpServletResponse res)  
                                           throws ServletException,IOException {
        List<Employee> employees;
        try {
            employees = employeeService.getAllEmployeeDetails();
            req.setAttribute("employees",employees);
            RequestDispatcher rd = req.getRequestDispatcher("/jsp/employee/displayallemployeedetails.jsp");
            rd.forward(req,res);
        } catch(DaoException ex) {
            System.out.println(ex.getMessage());
        }                                                           
    }
    
    private void getEmployee(HttpServletRequest req, HttpServletResponse res) 
                                           throws ServletException,IOException {
        String action = req.getParameter("action");
        String empId = req.getParameter("empId");
        Employee employee = null;
        try {
            if(action.equals("display")) {
                employee = employeeService.getEmployeeModelById(empId);
                req.setAttribute("employee",employee);
                RequestDispatcher rd = req.getRequestDispatcher("/jsp/employee/displayemployeedetails.jsp");
                rd.forward(req,res);
            } else if(action.equals("edit")) {
                employee = employeeService.getEmployeeAndAddressById(empId);
                req.setAttribute("employee",employee);
                RequestDispatcher rd = req.getRequestDispatcher("/jsp/employee/updateemployeedetails.jsp");
                rd.forward(req,res);
            }
        } catch (DaoException ex) {
            System.out.println(ex.getMessage());
        }                   
    }

   /**
    * updateEmployee method updates employee details of particular employee by 
    * his 'empId'. It reads the fields and validates the record and then it 
    * updates Employee object.
    */
    @SuppressWarnings("unchecked")
    public void updateEmployee(HttpServletRequest req,HttpServletResponse res)  
                                           throws ServletException,IOException {             
        try {
            Date dateOfBirth = null ;
            Date dateOfJoining = null;
            String empCode = req.getParameter("empCode");
            String birthDate = req.getParameter("dateOfBirth");
            String joiningDate = req.getParameter("dateOfJoining");
            try {
                dateOfBirth = new SimpleDateFormat("yyyy-MM-dd").
                                                                parse(birthDate);  
                dateOfJoining = new SimpleDateFormat("yyyy-MM-dd").
                                                            parse(joiningDate);          
            } catch (ParseException ex) {
                
            }
            Employee employee = employeeService.getEmployeeAndAddressById(empCode);
            
            employee.setName(req.getParameter("empName"));
            employee.setEmailId(req.getParameter("emailId"));
            employee.setPhoneNo(req.getParameter("phoneNo"));
            employee.setDateOfBirth(dateOfBirth);
            employee.setDateOfJoining(dateOfJoining);
            
            Set<Address> addresses = employee.getAddresses();
            
            for(Address address : addresses) {
                if(address.getIsPermanent()) {
                    address.setFlatNo(req.getParameter("p_flatNo"));
                    address.setStreetName(req.getParameter("p_streetName"));
                    address.setCity(req.getParameter("p_city"));
                    address.setState(req.getParameter("p_state"));
                    address.setZipCode(req.getParameter("p_zipCode"));   
                } else {
                    address.setFlatNo(req.getParameter("t_flatNo"));
                    address.setStreetName(req.getParameter("t_streetName"));
                    address.setCity(req.getParameter("t_city"));
                    address.setState(req.getParameter("t_state"));
                    address.setZipCode(req.getParameter("t_zipCode"));   
                }
            }
            employeeService.modifyEmployee(employee);
            req.setAttribute("message","Employee Details Updated successfully");
            this.getAllEmployees(req,res);
        } catch (DaoException ex) {
            System.out.println(ex.getMessage());
        }
    }

   /**
    * removeEmployee method removes employee details using empId.    
    */
    @SuppressWarnings("unchecked")
    public void deleteEmployee(HttpServletRequest req,HttpServletResponse res)  
                                           throws ServletException,IOException {
        String empId = req.getParameter("empId");
        try {
            employeeService.removeEmployeeById(empId);
            req.setAttribute("message","Employee Details Deleted successfully");
            this.getAllEmployees(req,res);
        } catch (DaoException ex) {
            System.out.println(ex.getMessage());
        } catch (InputMismatchException ex) {
            System.out.println("\n-----EmpId allows only Integer(0-9)------\n");
        } 
    }
}
