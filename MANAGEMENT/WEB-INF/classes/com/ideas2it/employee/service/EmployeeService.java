package com.ideas2it.employee.service;

import java.util.List;

import org.json.simple.JSONObject;

import com.ideas2it.employee.model.Employee;
import com.ideas2it.exception.DaoException;
import com.ideas2it.exception.UserDataException;



/**
 * EmployeeService interface provides basic crud operations methods, which needs
 * needs to implement, while implement this interface.
 * @author Vimalraj J
 * @date 29/08/2017
 */
public interface EmployeeService {

   /**
    * AddEmployeeDetails method get employee details as personal detail ,
    * permanent address, current address details in JSON Object.This methods  
    * stores the personal details in Employee pojo Object. and send over DAO 
    * Layer to store in database
    *
    * @param PersonalDetail - provides basic Employee details like id,name,etc..
    * @param permanentAddress / currentAddress - provides Address details like
    * flat no,street,etc..
    *
    * @throws DaoException - it's acustom Exception, occurs while Database 
    * Manipulation
    * 
    * @return it returns true when Employee details are successfully added or 
    * else returns false.
    */
	void addEmployeeDetails(Employee employee) throws DaoException, UserDataException;
   
   /**
    * getAllEmployeeDetails method gets Employee details from database as List of
    * Employee Model . It stores the list of address to to corresponding 
    * Employee pojo objects.
    *
    * @throws DaoException - it's acustom Exception, occurs while Database 
    * Manipulation
    *
    * @return - it returns all the Employee details as list of Employee 
    * Model objects.
    */
	List<Employee> getAllEmployeeDetails() throws DaoException;

   /**
    * modifyEmployee method updates Employee records(personal/Address details)
    * using Employee model object. This Object contains Edited Personal/ Address
    * detail .
    * @param employee model Object contains Employee personal details/list of
    * Addresses for particular customer
    *
    * @throws DaoException - it's acustom Exception, occurs while Database 
    * Manipulation
    *
    * @return returns true when all the employee details are properly updated
    * otherwise returns false 
    */
    @SuppressWarnings("unchecked")
	void modifyEmployee(Employee employee) throws DaoException;

   /**
    * removeEmployeeById remove a particular Employee details by his 'empCode'
    * @param empCode EmployeeId --- Unique Id to remove  particular employee
    * details
    *
    * @throws DaoException - it's a custom Exception, occurs while Database 
    * Manipulation
    *
    */
	void removeEmployeeById(String empCode) throws DaoException;
    
    /**
    * getEmployeeModelById method gets employee Model object of particular empCode
    * @param empCode - unique Id to refer Employee
    *
    * @throws DaoException - it's acustom Exception, occurs while Database 
    * Manipulation
    *
    * @return employee - Employee model Object Contains Employee details/
    * address details of Particular customer
    */
    public Employee getEmployeeModelById(String empCode) throws DaoException;
    
   /**
    * getEmployeeAndAddressById method gets employee Model object of particular empCode
    * @param empCode - unique Id to refer Employee
    *
    * @throws DaoException - it's acustom Exception, occurs while Database 
    * Manipulation
    *
    * @return employee - Employee model Object Contains Employee details/
    * address details of Particular customer
    */
	Employee getEmployeeAndAddressById(String empCode) throws DaoException;
	
	/**
    * getEmployeeProjectsById method gets employee Model object of particular empCode
    * @param empCode - unique Id to refer Employee
    *
    * @throws DaoException - it's acustom Exception, occurs while Database 
    * Manipulation
    *
    * @return employee - Employee model Object Contains Employee details/
    * address details of Particular customer
    */
	Employee getEmployeeProjectsById(String empCode) throws DaoException;
}
