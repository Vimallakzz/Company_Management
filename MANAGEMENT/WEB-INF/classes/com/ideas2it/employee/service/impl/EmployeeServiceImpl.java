package com.ideas2it.employee.service.impl;

import java.lang.Boolean;
import java.text.ParseException;
import java.text.SimpleDateFormat;  
import java.util.Date;
import java.util.List;
import java.util.LinkedList;
import java.util.Set;
import java.util.HashSet;
import org.json.simple.JSONObject;

import com.ideas2it.address.model.Address;
import com.ideas2it.address.service.AddressService;
import com.ideas2it.address.service.impl.AddressServiceImpl;
import com.ideas2it.common.Constants;
import com.ideas2it.common.CommonDao;
import com.ideas2it.employee.Dao.EmployeeDao;
import com.ideas2it.employee.Dao.impl.EmployeeDaoImpl;
import com.ideas2it.employee.model.Employee;
import com.ideas2it.employee.service.EmployeeService;
import com.ideas2it.exception.DaoException;
import com.ideas2it.exception.UserDataException;


/**
 * EmployeeServiceImpl class implementing EmployeeService interface.It serves  
 * crud operations(create/read/update/delete) on Employee details.
 * This class stores the list of Employee details in POJO objects and it stored 
 * in collection objects to retrive those information.
 * @Author Vimalraj.J
 * @Date 29/08/2017
 */
public class EmployeeServiceImpl implements EmployeeService {
    private EmployeeDao employeeDao = new EmployeeDaoImpl();
    private AddressService addressService = new AddressServiceImpl(); 
  
   /**
    * @see com.ideas2it.employee.service.EmployeeService 
    * #method addEmployeeDetails
    */
    @SuppressWarnings("unchecked")
    public void addEmployeeDetails(Employee employee) 
                                throws DaoException, UserDataException {
        String emailId =  employee.getEmailId();
        String phoneNo =  employee.getPhoneNo();
        JSONObject errorFields = employeeDao.
                                        validateEmailIdPhoneNo(emailId,phoneNo);
        if(errorFields == null) {
            employeeDao.insertEmployeeDetails(employee);
        } else {
            throw new UserDataException("Failed to add Employee Details due to:",
                                                                   errorFields);
        }                             
        
    }                  
    
   /**
    * addPersonalDetails methods adds the personalDetails  Employee Pojo object.
    * This Object is send to Dao layer to store in database . It gets 
    * Employee Code from database and returns the Employee Object 
    *
    * @param personalDetail - contains Employee basic details (id,name,etc)
    * @return returns Employee pojo object.
    * @return returns null, if EmailID or phoneNo is already exists in database
    */   
    private Employee addPersonalDetails(JSONObject personalDetail) 
                                                           throws DaoException {
        Employee employee = null;
        String name = (String) personalDetail.get("name");
        String emailId = (String) personalDetail.get("emailId");
        String phoneNo = (String) personalDetail.get("phoneNo");
        String birthDate = (String) personalDetail.get("dateOfBirth");
        String joiningDate = (String) personalDetail.get("dateOfJoining");
        Date dateOfBirth = null ;
        Date dateOfJoining = null;
        try {
            dateOfBirth = new SimpleDateFormat("yyyy-MM-dd").
                                                            parse(birthDate);  
            dateOfJoining = new SimpleDateFormat("yyyy-MM-dd").
                                                            parse(joiningDate);          
        } catch (ParseException ex) {
            
        }
        employee = new Employee(name, emailId, phoneNo, dateOfBirth,
                                                   dateOfJoining, Boolean.TRUE);
        return employee;
    }
  
   /**
    * @see com.ideas2it.employee.service.EmployeeService 
    * #method displayEmployeeDetails()
    */
    @SuppressWarnings("unchecked")
    public  List<Employee> getAllEmployeeDetails() throws DaoException {
        List<Employee> employees = employeeDao.getAllEmployeeDetails();
        return employees;
    }

   /**
    * @see com.ideas2it.employee.service.EmployeeService 
    * #method modifyEmployee()
    */
    @SuppressWarnings("unchecked")
    public void modifyEmployee(Employee employee) throws DaoException {       
        employeeDao.updateEmployeeDetails(employee);
    }
    
   /**
    * @see com.ideas2it.employee.service.EmployeeService 
    * #method removeEmployeeDetails()
    */
    @SuppressWarnings("unchecked")
    public  void removeEmployeeById(String empCode) throws DaoException {
        employeeDao.removeEmployee(empCode);
    }
  
  /**
   * @see com.ideas2it.employee.service.EmployeeService 
   * #method getEmployeeModelById(String)
   */
    public Employee getEmployeeModelById(String empCode) throws DaoException {
        return employeeDao.getEmployeeModelById(empCode);
    }
    
  /**
   * @see com.ideas2it.employee.service.EmployeeService 
   * #method getEmployeeAndAddressById(String)
   */
    public Employee getEmployeeAndAddressById(String empCode) throws DaoException {
        return employeeDao.getEmployeeAndAddressById(empCode);
    }
    
    /**
   * @see com.ideas2it.employee.service.EmployeeService 
   * #method getEmployeeProjectsById(String)
   */
    public Employee getEmployeeProjectsById(String empCode) throws DaoException {
        return employeeDao.getEmployeeProjectsById(empCode);
    }
    
}
