package com.ideas2it.employeeProject.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.ideas2it.employeeProject.model.EmployeeProject;
import com.ideas2it.employeeProject.service.EmployeeProjectService;
import com.ideas2it.employeeProject.Dao.EmployeeProjectDao;
import com.ideas2it.employeeProject.Dao.impl.EmployeeProjectDaoImpl;
import com.ideas2it.employee.model.Employee;
import com.ideas2it.employee.service.EmployeeService;
import com.ideas2it.employee.service.impl.EmployeeServiceImpl;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.exception.DaoException;
import com.ideas2it.project.model.Project;
import com.ideas2it.project.service.impl.ProjectServiceImpl;
import com.ideas2it.project.service.ProjectService;

/**
 * EmployeeProjectServiceImpl class implements EmployeeProjectService interface
 * it provides adding /deleting Employee-project association
 * @author Vimalraj J
 * @Date 31/08/2017
 */
public class EmployeeProjectServiceImpl implements EmployeeProjectService {
    private EmployeeService employeeService = new EmployeeServiceImpl();
    private ProjectService projectService = new ProjectServiceImpl();
    private EmployeeProjectDao employeeProjectDao = 
                                                   new EmployeeProjectDaoImpl();
   /**
    * @see com.ideas2it.employeeProject.service.EmployeeProjectService
    * #method assignProjects
    */ 
    public void assignProjects(String empCode, String projectCodes[]) 
                                                           throws DaoException {
        int flag = 1;
        Employee employee = employeeService.getEmployeeModelById(empCode);
        Set<Project> projects = employee.getProjects();
        Set<Project> removedProjects = new HashSet<Project>(); 
        if(projects == null ) {
            projects = new HashSet<Project>();     
        }
        
        if (projectCodes != null) {
            
            for(String projectCode : projectCodes) {              
                for(Project project : projects) {
                    if(project.getProjectCode().equals(projectCode)) {
                        flag = 0;
                        break;
                    }
                }
                if(flag==1) {
                    Project project = projectService.getProjectClientById(projectCode);
                    projects.add(project);
                } else {
                    flag = 1;
                }  
            }
            
            for(Project project : projects) {
                for(String projectCode : projectCodes) {
                    if(project.getProjectCode().equals(projectCode)) {
                        flag = 0;
                        break;
                    }
                }
                if(flag == 1) {
                    removedProjects.add(project);
                } else {
                    flag = 1;
                }  
            }           
            for(Project project : removedProjects) {
                projects.remove(project);
            }
        } else {
            projects.clear();
        }    
        employee.setProjects(projects);
        employeeProjectDao.assignEmployeeProject(employee);
    }
   
   /**
    * @see com.ideas2it.employeeProject.service.EmployeeProjectService
    * #method assignEmployees
    */ 
    public void assignEmployees(String projectCode, String empCodes[]) 
                                                           throws DaoException {
        int flag = 1;
        Project project = projectService.getProjectModelById(projectCode);
        Set<Employee> employees = project.getEmployees();
        Set<Employee> removedEmployees = new HashSet<Employee>(); 
        if(employees == null) {
            employees = new HashSet<Employee>(); 
        }
        if(empCodes != null) {
            for(String empCode : empCodes) {
                for(Employee employee : employees) {
                    if(employee.getEmpCode().equals(empCode)) {
                        flag = 0;
                        break;
                    }
                }
                if(flag==1) {
                    Employee employee = employeeService.getEmployeeAndAddressById(empCode);
                    employees.add(employee);
                } else {
                    flag = 1;
                } 
            }
            for(Employee employee : employees) {
                for(String empCode : empCodes) {
                    if(employee.getEmpCode().equals(empCode)) {
                        flag = 0;
                        break;
                    }
                }
                if(flag==1) {
                    removedEmployees.add(employee);
                } else {
                    flag = 1;
                } 
            }
            for(Employee employee : removedEmployees) {
                employees.remove(employee);
            }
        } else {
            employees.clear();
        }
        project.setEmployees(employees);
        employeeProjectDao.assignEmployeeProject(project);
    }
}

