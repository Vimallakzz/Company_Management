package com.ideas2it.employeeProject.service;

import java.util.Set;

import com.ideas2it.employee.model.Employee;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.exception.DaoException;
import com.ideas2it.project.model.Project;

/**
 * EmployeeProjectService interface provides basic crud operations methods, 
 * which needs to implement, while implement this interface.
 * @author Vimalraj J
 * @date 31/08/2017
 */
public interface EmployeeProjectService {
    
   void assignProjects(String empCode, String projectCodes[]) 
                                                            throws DaoException;
   
   void assignEmployees(String projecCode, String empCodes[]) 
                                                            throws DaoException;           
} 
