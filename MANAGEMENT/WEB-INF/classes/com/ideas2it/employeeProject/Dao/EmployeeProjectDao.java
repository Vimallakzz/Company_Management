package com.ideas2it.employeeProject.Dao;

import java.util.List;

import com.ideas2it.employee.model.Employee;
import com.ideas2it.exception.DaoException;
import com.ideas2it.project.model.Project;

/**
 * EmployeeProjectDao interface provides methods which needs to implement for 
 * Employee-project Association
 * @author Vimalraj J
 * @Date 4/09/2017
 */
public interface EmployeeProjectDao {

   /**
    * assignEmployeeProject methods assign Employee to Project
    *
    * @param employeeProject of type EmployeeProject
    *
    * @throws DaoException - It's a custom Exception ,occurs while Database 
    * Manipulation
    */
    void assignEmployeeProject(Employee employee) throws DaoException;
    
    /**
    * assignEmployeeProject methods assign Employee to Project
    *
    * @param employeeProject of type EmployeeProject
    *
    * @throws DaoException - It's a custom Exception ,occurs while Database 
    * Manipulation
    */
    void assignEmployeeProject(Project project) throws DaoException;
    
    /**
    * deAssignEmployeeProject() method disassociated the given employee 
    * from the given project.
    *
    * @param empId - its unique id to find out the Particular Employee.
    * @param projectId - its unique id to find out the Particular Project.
    *
    * @throws DaoException - It's a custom Exception ,occurs while Database 
    * Manipulation
    */
    void deAssignEmployeeProject(Employee employee) throws DaoException;
}
