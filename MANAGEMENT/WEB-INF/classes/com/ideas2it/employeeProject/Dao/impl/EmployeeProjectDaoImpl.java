package com.ideas2it.employeeProject.Dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.LinkedList; 
import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.ideas2it.common.ConnectionFactory;
import com.ideas2it.employee.model.Employee;
import com.ideas2it.employeeProject.Dao.EmployeeProjectDao;
import com.ideas2it.employeeProject.model.EmployeeProject;
import com.ideas2it.exception.DaoException;
import com.ideas2it.logger.ApplicationLogger;
import com.ideas2it.project.model.Project;
import hibernate.sessionfactory.CreateSessionFactory;

/**
 * EmployeeProjectDaoImpl class implements EmployeeProjectDao interface , and 
 * provides necessary methods for Employee-project crud Operations 
 * @author Vimalraj.J
 * @Date 4/09/2017
 */
public class EmployeeProjectDaoImpl implements EmployeeProjectDao {
    private SessionFactory sessionFactory = CreateSessionFactory.getInstance();
    static {
        ApplicationLogger.setLoggerInstance();
    }
    
   /**
    * @see com.ideas2it.employeeProject.Dao.EmployeeProjectDao
    * #method  assignEmployeeProject()
    */
    public void assignEmployeeProject(Employee employee) throws DaoException {
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            session.update(employee);
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }
            ApplicationLogger.error("Error while assign Employee to Project "+
                                    "where employeeDetails be :"+ employee, ex);
            throw new DaoException("Something went Wrong ...Please try again "+
                                   "Later...");
        } finally {
            session.close();
        }
    }
    
     /**
    * @see com.ideas2it.employeeProject.Dao.EmployeeProjectDao
    * #method  assignEmployeeProject()
    */
    public void assignEmployeeProject(Project project) throws DaoException {
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            session.update(project);
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }
            ApplicationLogger.error("Error while assign Employee to Project "+
                                    "where employeeDetails be :"+ project, ex);
            throw new DaoException("Something went Wrong ...Please try again "+
                                   "Later...");
        } finally {
            session.close();
        }
    }
    
    /**
    * @see com.ideas2it.employeeProject.Dao.EmployeeProjectDao
    * #method  deAssignEmployeeProject()
    */
     public void deAssignEmployeeProject(Employee employee) 
                                                           throws DaoException {
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            session.update(employee);
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }
            ApplicationLogger.error("Error while assign Employee to Project "+
                                    "where employeeDetails be :"+ employee, ex);
            throw new DaoException("Something went Wrong ...Please try again "+
                                   "Later...");
        } finally {
            session.close();
        }
     }
}
