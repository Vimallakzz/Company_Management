package com.ideas2it.employeeProject.controller;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;

import com.ideas2it.common.Constants;
import com.ideas2it.employee.model.Employee;
import com.ideas2it.employee.service.EmployeeService;
import com.ideas2it.employee.service.impl.EmployeeServiceImpl;
import com.ideas2it.employeeProject.service.EmployeeProjectService;
import com.ideas2it.employeeProject.service.impl.EmployeeProjectServiceImpl;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.exception.DaoException;
import com.ideas2it.project.model.Project;
import com.ideas2it.project.service.impl.ProjectServiceImpl;
import com.ideas2it.project.service.ProjectService;

/**
 * EmployeeProjectController class provides crud opertions to be done on 
 * Employee- project association .
 * @author Vimalraj
 * @Date 31/08/2017
 */
public class EmployeeProjectController extends HttpServlet { 
    private EmployeeService employeeService = new EmployeeServiceImpl();
    private ProjectService projectService = new ProjectServiceImpl();
    private EmployeeProjectService employeeProjectService = 
                                               new EmployeeProjectServiceImpl();
    
    public boolean isSessionExpired(HttpServletRequest req, 
                  HttpServletResponse res) throws ServletException,IOException {
        HttpSession session =  req.getSession(false);
        if(session != null && session.getAttribute("userName") != null) {
            return true;  
        }
        return false;
    }
    
    public void doGet(HttpServletRequest req,HttpServletResponse res)  
                                           throws ServletException,IOException {
        boolean status = this.isSessionExpired(req,res);
        if(status) {
            String parameter = req.getParameter("parameter");
            if(parameter != null ) {
                if(parameter.equals("assignProjects")) {
                    this.assignProjects(req,res);
                } else if(parameter.equals("assignEmployees")) {
                    this.assignEmployees(req,res);
                } else {
                    res.sendError(503);
                }
            } else {
                res.sendError(503);
            }
        } else {
        res.sendRedirect("/MANAGEMENT/html/login.html");
        }
            
            
    
    } 
    
    public void doPost(HttpServletRequest req,HttpServletResponse res)  
                                           throws ServletException,IOException {
        boolean status = this.isSessionExpired(req,res);
        if(status) {
            String parameter = req.getParameter("parameter");
            if(parameter != null ) {
                if(parameter.equals("assignProjects")) {
                    this.assignProjects(req,res);
                } else if(parameter.equals("assignEmployees")) {
                    this.assignEmployees(req,res);
                } else {
                    res.sendError(503);
                }
            } else {
                res.sendError(503);
            }
        } else {
        res.sendRedirect("/MANAGEMENT/html/login.html");
        }
    }                                 
                                                   
  
    
   /**
    * assignEmployees() method asssigns valid employee to the given project.
    * Condition to be employee can max assign to 3 projects
    * One project Contains max of 5 members
    *
    * @param projectId -- to identify the particular Project Information to 
    * associate to Employees...
    */
    public void assignEmployees(HttpServletRequest req,HttpServletResponse res)  
                                           throws ServletException,IOException {
        String projectCode = req.getParameter("projectId");
        String empCodes[] = req.getParameterValues("empId");
        try {
            employeeProjectService.assignEmployees(projectCode,empCodes);
            Project project = projectService.getProjectModelById(projectCode);
            req.setAttribute("project", project);
            req.setAttribute("message","Employee - Project Associated/DisAssociated Successfully  ");
            RequestDispatcher rd = req.getRequestDispatcher("/jsp/project/displayprojectdetails.jsp");
            rd.forward(req,res);
        } catch (DaoException ex) {
            System.out.println(ex.getMessage());
        }  
    }
    
    /**
    * assignProjects() method asssigns valid project to the given employee.
    * Condition to be employee can max assign to 3 projects
    * One project Contains max of 5 members
    *
    * @param empId to identify the particular Employee  to associate Projects.....
    */
    public void assignProjects(HttpServletRequest req,HttpServletResponse res)  
                                           throws ServletException,IOException {
        String empCode = req.getParameter("empId");
        String projectCodes[] = req.getParameterValues("projectId");
        try {
            employeeProjectService.assignProjects(empCode,projectCodes);
            Employee employee = employeeService.getEmployeeModelById(empCode);
            req.setAttribute("employee",employee);
            req.setAttribute("message","Employee - Project Associated/DisAssociated Successfully  ");
            RequestDispatcher rd = req.getRequestDispatcher("/jsp/employee/displayemployeedetails.jsp");
            rd.forward(req,res);
        } catch (DaoException ex) {
            System.out.println(ex.getMessage());
        }
    }

  
}
