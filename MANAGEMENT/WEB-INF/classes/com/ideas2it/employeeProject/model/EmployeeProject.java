package com.ideas2it.employeeProject.model;

import com.ideas2it.employee.model.Employee;
import com.ideas2it.project.model.Project;

/**
 * EmployeeProject model class provides setters / getters to store Employee and 
 * project pojo objects . This class use to associate Employee to project.
 * Association may be one to one/ one to many / many to many.In this Employee 
 * Project Association follows many to many.
 * @author Vimalraj J
 * @date 31/08/2017
 */
public class EmployeeProject {
    private Employee employeeDetail;
    private Project projectDetail;

    public EmployeeProject() {
    
    }

    public EmployeeProject(Employee employeeDetail,Project projectDetail) {
        this.employeeDetail = employeeDetail;
        this.projectDetail = projectDetail;    
    }
    
    public void  setEmployee(Employee employeeDetail) {
        this.employeeDetail = employeeDetail;
    }

    public void setProject(Project projectDetail) {
        this.projectDetail = projectDetail;
    }

    public Project getProject() {
        return this.projectDetail;
    }

    public Employee getEmployee() {
        return this.employeeDetail;
    }
}

