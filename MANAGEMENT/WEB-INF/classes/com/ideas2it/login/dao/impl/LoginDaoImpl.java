package com.ideas2it.login.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.FetchMode;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.Query;

import com.ideas2it.login.dao.LoginDao;
import com.ideas2it.login.model.Login;
import hibernate.sessionfactory.CreateSessionFactory;
import com.ideas2it.exception.DaoException;
import com.ideas2it.logger.ApplicationLogger;

/**
 * ClientDaoImpl class implements ClientDao methods provides necessary crud
 * operations .
 * @author VimalRaj J
 * @Date 01-09-2017
 */
public class LoginDaoImpl implements LoginDao {
    private SessionFactory sessionFactory = CreateSessionFactory.getInstance();
    static {
        ApplicationLogger.setLoggerInstance();
    }
    
   /**
    * @see com.ideas2it.client.Dao.ClientDao 
    * #method insertClientDetails()
    */
    @SuppressWarnings({"deprecation","unchecked"})
    public void insertLoginDetails(Login loginDetail) throws DaoException { 
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
           
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }
            ApplicationLogger.error("", ex);
            throw new DaoException();
        } finally {
            session.close();
        }
    }
   
   
   
  
}
