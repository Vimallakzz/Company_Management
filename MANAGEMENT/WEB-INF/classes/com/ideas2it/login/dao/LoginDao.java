package com.ideas2it.login.dao;

import com.ideas2it.login.model.Login;
import com.ideas2it.exception.DaoException;

/*
 * ClientDao interface provides database crud operations with some necessary 
 * methods 
 * @author Vimalraj.J
 * @Date 01-09-2017
 */
public interface LoginDao {
    
    void insertLoginDetails(Login loginDetail) throws DaoException;
   
}
