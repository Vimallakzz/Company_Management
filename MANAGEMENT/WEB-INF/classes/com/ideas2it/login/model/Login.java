package com.ideas2it.login.model;

import java.util.Set;

import com.ideas2it.address.model.Address;
import com.ideas2it.project.model.Project;

/**
 * Login pojo class provides getters and Setters for basic login details 
 * like userName,password.
 * This class also overrides (toString() method of Object class.
 * @Author Vimalraj J
 * @Date 28/08/2017
 */
public class Login {
    private int id;
    private String userName;
    private String password;
    
    public Login() {
    
    }
    
    public Login(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }
    
    public void setId() {
        this.id = id;
    }
    
    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
    public int getId() {
        return this.id;
    }
    
    public String getUserName(){
        return this.userName;
    }
    
    
    public String getPassword() {
        return this.password;
    }    
}
