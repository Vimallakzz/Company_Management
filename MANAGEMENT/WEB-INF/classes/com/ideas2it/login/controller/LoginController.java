package com.ideas2it.login.controller;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;

import com.ideas2it.login.service.LoginService;
import com.ideas2it.login.service.impl.LoginServiceImpl;
/**
 * EmployeeController class provides an option for 
 * Crud operations(read/update/delete/create) .
 * @Author Vimalraj 
 * @Date   29/08/2017 
 */
public class LoginController extends HttpServlet { 
    private LoginService loginService = new LoginServiceImpl();
    
    public void doGet(HttpServletRequest req,HttpServletResponse res)  
                                           throws ServletException,IOException {
        String parameter = req.getParameter("parameter");
        if(parameter != null) {
            if(parameter.equals("login")) {
                this.checkLogInValidation(req,res);
            } else if(parameter.equals("signup")) {
                this.addLoginDetails(req,res);
            } else if(parameter.equals("logout")) {
                this.logOutUserSession(req,res);
            } 
        } else {
            res.sendRedirect("/MANAGEMENT/html/login.html");
        }
    }
    
    public void doPost(HttpServletRequest req,HttpServletResponse res)  
                                           throws ServletException,IOException {
        String parameter = req.getParameter("parameter");
        if(parameter != null) {
            if(parameter.equals("login")) {
                this.checkLogInValidation(req,res);
            } else if(parameter.equals("signup")) {
                this.addLoginDetails(req,res);
            } else if(parameter.equals("logout")) {
                this.logOutUserSession(req,res);
            } 
        } else {
            res.sendRedirect("/MANAGEMENT/html/login.html");
        }
    }

   /**
    * addLoginDetails method login information from
    * User to add the login details in database
    */
    @SuppressWarnings("unchecked")
    public  void addLoginDetails(HttpServletRequest req,HttpServletResponse res)  
                                           throws ServletException,IOException {
        
    }
    
   /**
    * addLoginDetails method login information from
    * User to add the login details in database
    */
    public  void checkLogInValidation(HttpServletRequest req, 
                  HttpServletResponse res) throws ServletException,IOException {
        String userName = req.getParameter("userName");
        String password = req.getParameter("password");
        if(userName .equals("admin") & password.equals("admin")) {
            System.out.println("---------Login Successfully------------------");
            HttpSession session = req.getSession();
            session.setAttribute("userName",userName);
            RequestDispatcher rd = req.getRequestDispatcher("/employeeController?parameter=getAllEmployees");
            rd.forward(req,res);
        }
    } 
    
    public void logOutUserSession(HttpServletRequest req,HttpServletResponse res)  
                                           throws ServletException,IOException {
        HttpSession session = req.getSession(false);
        if(session != null) {
            session.invalidate();
        }
        res.sendRedirect("/MANAGEMENT/html/login.html");
    }
}
