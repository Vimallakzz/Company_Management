package com.ideas2it.client.controller;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import org.json.simple.JSONObject;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;

import com.ideas2it.address.model.Address;
import com.ideas2it.client.model.Client;
import com.ideas2it.client.service.ClientService;
import com.ideas2it.client.service.impl.ClientServiceImpl;
import com.ideas2it.common.Constants;
import com.ideas2it.exception.DaoException;
import com.ideas2it.exception.UserDataException;
import com.ideas2it.project.model.Project;

/**
 * ClientController class provides an option for 
 * Crud operations(read/update/delete/create) .
 * @Author Vimalraj 
 * @Date   29/08/2017 
 */
public class ClientController extends HttpServlet {
    private ClientService clientService = new ClientServiceImpl();
    private Scanner scan = new Scanner(System.in);
    
    public void doGet(HttpServletRequest req,HttpServletResponse res)  
                                           throws ServletException,IOException {
        String parameter = req.getParameter("parameter");
        if(parameter != null) {
            if (parameter.equals("addClientDetails")) {
                this.addClient(req,res);
            } else if (parameter.equals("getAllClients")) {
                this.getAllClients(req,res);
            } else if (parameter.equals("getClient")) {
                this.getClient(req,res);
            } else if (parameter.equals("updateClientDetails")) {
                this.updateClient(req,res);
            }else if (parameter.equals("deleteClient")) {
                this.removeClient(req,res);
            } else {
                res.sendError(503);
            }
        } else {
            res.sendError(503);
        }
         
    
    }
    
    public void doPost(HttpServletRequest req,HttpServletResponse res)  
                                           throws ServletException,IOException {
        String parameter = req.getParameter("parameter");
        if(parameter != null) {
            if (parameter.equals("addClientDetails")) {
                this.addClient(req,res);
            } else if (parameter.equals("getAllClients")) {
                this.getAllClients(req,res);
            } else if (parameter.equals("getClient")) {
                this.getClient(req,res);
            } else if (parameter.equals("updateClientDetails")) {
                this.updateClient(req,res);
            }else if (parameter.equals("deleteClient")) {
                this.removeClient(req,res);
            } else {
                res.sendError(503);
            }
        } else {
            res.sendError(503);
        }
    }
    
   /**
    * addClient method gets Personal Information / Address information from
    * User to add the Client details in database
    */
    public void addClient(HttpServletRequest req,HttpServletResponse res)  
                                           throws ServletException,IOException {
        JSONObject currentAddress = null;
        JSONObject personalDetail = this.readPersonalDetailsFromRequest(req,res);
        JSONObject permanentAddress = this.getAddressDetailsFromRequest(req,res,true);
        
        String isSameAsPermanent = req.getParameter("isSameAsPermanent");
        if(isSameAsPermanent != null && isSameAsPermanent.equals("yes")) {
            currentAddress = permanentAddress;
        } else {
            currentAddress = this.getAddressDetailsFromRequest(req,res,false);
        }    
        try {
            clientService.addClientDetails(personalDetail, permanentAddress, 
                                                                currentAddress);
            req.setAttribute("message","Client Details Added Successfully");
            this.getAllClients(req,res);
        } catch (DaoException ex) {
            System.out.println(ex.getMessage());
        } catch (UserDataException ex) {
            System.out.println(ex.getMessage() + " " + ex.getErroredUserData());
        }                       
    }
    
    /**
    * getclientDetailsFromUser method gets client details from user and 
    * stores the details in JSON object
    *
    * @return it returns Json object contains details of one client 
    */
    @SuppressWarnings("unchecked")
    public  JSONObject readPersonalDetailsFromRequest(HttpServletRequest req,
                                                      HttpServletResponse res) 
                                           throws ServletException,IOException {                               
        String name = req.getParameter("clientName");
        String emailId = req.getParameter("emailId");
        String phoneNo = req.getParameter("phoneNo");
        
        JSONObject clientDetail = new JSONObject();
        clientDetail.put("name",name);
        clientDetail.put("emailId",emailId);
        clientDetail.put("phoneNo",phoneNo);
        return clientDetail; 
    }
    
   /**
    * getAddressDetailsFromRequest method gets Address Detail from User and returns
    * Address Details in form of Json
    *
    * @return it returns address detail in key value pair stored in JSONObject
    */ 
    public JSONObject getAddressDetailsFromRequest(HttpServletRequest req, 
                                                   HttpServletResponse res, boolean status)  
                                           throws ServletException,IOException {
        JSONObject clientAddress = new JSONObject();
        String subString = null;
        // if status is false then read temporary address details otherwise read permanent addressDetails
        if(!status) {
            String flatNo = req.getParameter("t_flatNo");
            String streetName = req.getParameter("t_streetName");
            String city = req.getParameter("t_city");
            String state = req.getParameter("t_state");
            String zipCode = req.getParameter("t_zipCode");
            
            clientAddress.put("flatNo",flatNo);
            clientAddress.put("streetName",streetName);
            clientAddress.put("city",city);
            clientAddress.put("state",state);
            clientAddress.put("zipCode",zipCode);
            clientAddress.put("isPermanent",false);
        } else {
            String flatNo = req.getParameter("p_flatNo");
            String streetName = req.getParameter("p_streetName");
            String city = req.getParameter("p_city");
            String state = req.getParameter("p_state");
            String zipCode = req.getParameter("p_zipCode");
            
            clientAddress.put("flatNo",flatNo);
            clientAddress.put("streetName",streetName);
            clientAddress.put("city",city);
            clientAddress.put("state",state);
            clientAddress.put("zipCode",zipCode);
            clientAddress.put("isPermanent",true);
        }                       
        
        
                
        return clientAddress;
    }
    
   /**
    * displayClient method display all Client Personal Details and Address
    * Details .
    */
    public void getAllClients(HttpServletRequest req,HttpServletResponse res)  
                                           throws ServletException,IOException {
        List<Client> clients = null;
        try {
            clients = clientService.getAllClientDetails();
            req.setAttribute("clients",clients);
            RequestDispatcher rd = req.getRequestDispatcher("/jsp/client/displayallclientdetails.jsp");
            rd.forward(req,res);
        } catch (DaoException ex) {
            System.out.println(ex.getMessage());
        }
    }
    
    private void getClient(HttpServletRequest req, HttpServletResponse res) 
                                           throws ServletException,IOException {
        String action = req.getParameter("action");
        String clientId = req.getParameter("clientId");
        Client client = null;
        try {
            if(action.equals("display")) {
                client = clientService.getClientModelById(clientId);
                req.setAttribute("client", client);
                RequestDispatcher rd = req.getRequestDispatcher("/jsp/client/displayclientdetails.jsp");
                rd.forward(req,res);
            } else if(action.equals("edit")) {
                client = clientService.getClientAndAddressById(clientId);
                req.setAttribute("client", client);
                RequestDispatcher rd = req.getRequestDispatcher("/jsp/client/updateclientdetails.jsp");
                rd.forward(req,res);
            }
        } catch (DaoException ex) {
            System.out.println(ex.getMessage());
        }                   
    }
    
   /**
    * updateClient method updates client details of particular client by 
    * his 'clientId'. It reads the fields and validates the record and then it 
    * updates Client object.
    */
    public void updateClient(HttpServletRequest req,HttpServletResponse res)  
                                           throws ServletException,IOException {
        try {
            String clientCode = req.getParameter("clientCode");
            Client client = clientService.getClientAndAddressById(clientCode);
            
            client.setName(req.getParameter("empName"));
            client.setEmailId(req.getParameter("emailId"));
            client.setPhoneNo(req.getParameter("phoneNo"));
            
            Set<Address> addresses = client.getAddresses();
            
            for(Address address : addresses) {
                if(address.getIsPermanent()) {
                    address.setFlatNo(req.getParameter("p_flatNo"));
                    address.setStreetName(req.getParameter("p_streetName"));
                    address.setCity(req.getParameter("p_city"));
                    address.setState(req.getParameter("p_state"));
                    address.setZipCode(req.getParameter("p_zipCode"));   
                } else {
                    address.setFlatNo(req.getParameter("t_flatNo"));
                    address.setStreetName(req.getParameter("t_streetName"));
                    address.setCity(req.getParameter("t_city"));
                    address.setState(req.getParameter("t_state"));
                    address.setZipCode(req.getParameter("t_zipCode"));   
                }
            }
            clientService.modifyClient(client);
            req.setAttribute("message","Client Details Updated Successfully");
            this.getAllClients(req,res); 
        } catch (DaoException ex) {
            System.out.println(ex.getMessage());
        }
    }
    
   /**
    * removeClient method removes client details using clientId.    
    */
    public void removeClient(HttpServletRequest req,HttpServletResponse res)  
                                           throws ServletException,IOException {    
        String clientId = req.getParameter("clientId");
        try {
            clientService.removeClientById(clientId);
            req.setAttribute("message","Client Details Deleted Successfully");
            this.getAllClients(req,res); 
        } catch (DaoException ex) {
            System.out.println(ex.getMessage());
        } catch (InputMismatchException ex) {
            System.out.println("\n----clientId allows only Integer(0-9)----\n");
            scan.nextLine();    // to Eradicate '\n' charcter in stream, this 
                               // way to handle that charcter
        }  
    }
   /**
    * displayProjectsOfClient method display the Projects Of given clientId
    
    public void displayProjectsOfClient() {
        try {
            System.out.println("Enter the ClientId Which u want to list Of "+
                                                                    "Projects");
            String clientId = scan.nextInt() + "";
            boolean isAvailable = clientService.isClientAvailable(clientId);
            if (isAvailable) {
                Client client = clientService.getClientProjectsById(clientId);
                Set<Project> projects = client.getProjects();
                System.out.println("ClientCode \t ProjectCode \t Title \t "+
                                   "Description \t Domain");
                for (Project project : projects) {
                    if(project.getIsActive()) {
                        System.out.println("----------------------------------"+
                                           "---------------------------------");
                        System.out.println(project.toString());
                    }
                } 
            } else {
                System.out.println("\n\n-----Enter Valid ClientId-----\n\n");
            }
        } catch (DaoException ex) {
            System.out.println(ex.getMessage());
        } catch (InputMismatchException ex) {
            System.out.println("\n----clientId allows only Integer(0-9)----\n");
            scan.nextLine();    // to Eradicate '\n' charcter in stream, this 
                               // way to handle that charcter
        }  
            
    }*/
}


