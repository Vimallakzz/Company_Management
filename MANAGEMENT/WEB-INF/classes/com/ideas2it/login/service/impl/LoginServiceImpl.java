package com.ideas2it.login.service.impl;

import com.ideas2it.exception.DaoException;
import com.ideas2it.login.model.Login;
import com.ideas2it.login.service.LoginService;
import com.ideas2it.login.dao.LoginDao;
import com.ideas2it.login.dao.impl.LoginDaoImpl;

/**
 * ClientServiceImpl class implementing ClientService interface.It serves  
 * crud operations(create/read/update/delete) on Client details.
 * This class stores the list of Client details in POJO objects and it stored 
 * in collection objects to retrive those information.
 * @Author Vimalraj.J
 * @Date 29/08/2017
 */
public class LoginServiceImpl implements LoginService {
    
    private  LoginDao loginDao = new LoginDaoImpl();
    
    public void insertLoginDetails(String userName,String password) 
                                                           throws DaoException {
        Login login = new Login();
        loginDao.insertLoginDetails(login);
    }
}
