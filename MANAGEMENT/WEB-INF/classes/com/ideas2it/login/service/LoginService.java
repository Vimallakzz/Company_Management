package com.ideas2it.login.service;

import com.ideas2it.exception.DaoException;

/**
 * LoginService interface provides basic crud operations methods, which needs
 * needs to implement, while implement this interface.
 * @author Vimalraj J
 * @date 29/08/2017
 */
public interface LoginService {

    void insertLoginDetails(String userName,String password) 
                                                           throws DaoException;
   
}
