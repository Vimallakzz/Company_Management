package com.ideas2it.logger;

import java.io.File;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * ApplicationLogger class provides application level logger to store the logs 
 * which occurs in Application. Currently uses log4j.
 * Its very easy to change the logger in future by just changing in this file
 * @author Vimalraj.J
 * @date 16/09/2017 
 */
public class ApplicationLogger {

    private static Logger logger = null;
    public static void setLoggerInstance() {
        System.out.println("----------------------------");
        if(logger == null) {
            loadConfiguration();
            logger = Logger.getLogger(ApplicationLogger.class);
        }  
    }
    
    public static void loadConfiguration() {
        String loggerConfigFile = System.getProperty("user.home") +
                                  File.separator + "Desktop" +
                                  File.separator + "program" +
                                  File.separator + "apache-tomcat-8.5.20" +
                                  File.separator + "webapps" +
                                  File.separator + "MANAGEMENT" +
                                  File.separator + "WEB-INF" +
                                  File.separator + "classes" +
                                  File.separator + "com" + 
                                  File.separator + "ideas2it" + 
                                  File.separator + "logger" + 
                                  File.separator + "logger-configuration.xml";                    
		DOMConfigurator.configure(loggerConfigFile);     
    } 
    
    
    
    public static void debug(String message) {
        logger.debug(message);
    }
    
    public static void error(String message,Exception ex) {
        logger.error(message,ex);
    }
    
    public static  void info(String message) {
        logger.info(message);
    }  
}
