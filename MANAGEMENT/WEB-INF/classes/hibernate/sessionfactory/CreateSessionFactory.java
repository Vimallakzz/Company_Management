package hibernate.sessionfactory;

import org.hibernate.cfg.Configuration;
import org.hibernate.SessionFactory;

public class CreateSessionFactory {
    private static SessionFactory sessionFactory = null;
    
    private CreateSessionFactory() {
    
    }
    
    public static SessionFactory getInstance() {
        if (sessionFactory == null) {
            sessionFactory = new Configuration().
                         configure("hibernate/configuration/hibernate.cfg.xml").
                         buildSessionFactory();
        }
        return sessionFactory;
    }

}
