var modal = document.getElementById("modal");
function assignProjects(empCode) {
    document.getElementById("modal").style.display = 'block';
    var url = "employeeController?parameter=getEmployeeProjects&empId="+empCode;
    document.getElementById("myFrame").src = url;                     
}
        
var closeModal = document.getElementById("closeModal");

closeModal.onclick = function() {
    document.getElementById("modal").style.display = "none";
}

window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
