function enableSearchBox(columnNo, type) {  
    var tr = document.getElementById("search");
    tr.className = "show";
    var td = tr.getElementsByTagName("td");
    for(i = 0; i < td.length - 1;i++) {
        if(i == columnNo) {
            td[i].className = "show";
        } else {
            td[i].className = "hide";
        }
    }
    removeSearchValues();
    showAllDetails(type);    
}

function showAllDetails(type) {
    var table = document.getElementById(type);
    var tr = table.getElementsByTagName("tr");
    for(i = 0;i<tr.length;i++) {
         tr[i].className = "show";
    }
    document.getElementById("no_search_results").className = "hide";
}

function removeSearchValues() {
    var tr = document.getElementById("search");
    var td = tr.getElementsByTagName("td");
    for(i=0;i < td.length - 1;i++) {
        td[i].getElementsByTagName("input")[0].value = "";
    }
}

function hideSearchBar(type) {
    var table = document.getElementById(type);
    var tr = table.getElementsByTagName("tr");
    for(i = 0;i < tr.length;i++) {
         tr[i].className = "show";
    }
    removeSearchValues();
    document.getElementById("search").className = "hide";
    document.getElementById("no_search_results").className = "hide";
}
