function filterTableData(columnNo) {
    var filterValue;
    if(columnNo == 0) {
        filterValue = document.getElementById("searchEmpCode").value.toLowerCase();
    } else if(columnNo == 1) {
        filterValue = document.getElementById("searchName").value.toLowerCase();
    } else if(columnNo == 2) {
        filterValue = document.getElementById("searchEmail").value.toLowerCase();
    } else if(columnNo == 3) {
        filterValue = document.getElementById("searchMobileNo").value.toLowerCase();
    } else if(columnNo == 4) {
        filterValue = document.getElementById("searchDOB").value.toLowerCase();
    } else if(columnNo == 5) {
        filterValue = document.getElementById("searchDOJ").value.toLowerCase();
    } 
    var table = document.getElementById("employeeDetails");
    var tr = table.getElementsByTagName("tr");
    var td;
    var count = 0;
    for(i=0;i<tr.length;i++) {
        if(i > 2) {
            td = tr[i].getElementsByTagName("td")[columnNo];
            if(td) {
                if(columnNo != 0) {
                    columnValue = td.innerHTML.toLowerCase()
                } else {
                    columnValue = td.getElementsByTagName("a")[0].innerHTML.toLowerCase().substring(0,9);
                }
                if(columnValue.indexOf(filterValue) != -1) {
                    tr[i].className = "show";
                    count++;
                } else {
                    tr[i].className = "hide";
                }
            }
        }   
    }
    if(count == 0 ) {
        document.getElementById("no_search_results").className = "show";
    } else {
        document.getElementById("no_search_results").className = "hide";
    }
}
