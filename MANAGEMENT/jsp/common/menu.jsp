<!DOCTYPE html>
<html>
<head>
<style>
#mySidenav a {
    position: fixed;
    left: -10px;
    transition: 0.3s;
    padding: 15px;
    width: 15%;
    text-decoration: none;
    font-size: 20px;
    color: white;
    border-radius: 0 5px 5px 0;
    background-color: #555
}

#mySidenav a:hover {
    left: 0px;
}

#EmployeeManagement {
    top: 100px;
    
}

#displayEmployee {
    top: 160px;
}

#ProjectManagement {
    top: 220px;
}

#displayProject {
    top: 280px;
}

#ClientManagement {
    top: 340px;
}

#displayClient {
    top: 400px;
}

</style>
</head>
<body>

<div id="mySidenav" class="sidenav">
  <a href="/MANAGEMENT/jsp/employee/addemployeedetails.jsp" id="EmployeeManagement">EmployeeManagement</a>
  <a href="/MANAGEMENT/employeeController?parameter=getAllEmployees" id="displayEmployee">Display Employee</a>
  <a href="/MANAGEMENT/projectController?parameter=addClientProject" id="ProjectManagement">ProjectManagement</a>
  <a href="/MANAGEMENT/projectController?parameter=getAllProjects" id="displayProject">Display Project</a>
  <a href="/MANAGEMENT/jsp/client/addclientdetails.jsp" id="ClientManagement">ClientManagement</a>
  <a href="/MANAGEMENT/clientController?parameter=getAllClients" id="displayClient">Display Client</a>
</div>
     
</body>
</html> 

