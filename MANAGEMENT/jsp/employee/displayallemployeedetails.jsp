<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<html>
<head>
<title></title>
<link href = "/MANAGEMENT/css/table-style.css" rel = "stylesheet" type = "text/css">
<link href = "/MANAGEMENT/css/table-style-search.css" rel = "stylesheet" type = "text/css">
<link href = "/MANAGEMENT/css/tool-tip.css" rel = "stylesheet" type = "text/css">
<script src = "/MANAGEMENT/js/search.js"></script>
<script src = "/MANAGEMENT/js/employee-filter-data.js"></script>
<script>
var alertMessage = '${message}';
if(alertMessage != "") {
    alert(alertMessage);
}
</script>
</head>
<body>
    <table  border = "0" cellspacing = "0" cellpadding = "0" style = "width:100%;height:100%">
        <tr height = "8%"><td colspan = "2" ><%@ include file="../../jsp/common/header.jsp"%></td></tr>
        <tr height = "87%">
            <td width = "17%"><%@ include file="../../jsp/common/menu.jsp"%></td>
            <td width = "81%">
               <div class = "container-all">
                    <label class = "label">Employees-List :</label><br><br>
                        <table id = "employeeDetails" border = "0" cellspacing = "0" cellpadding = "0" class = "table-all table-position">        
                            <c:choose>
                                <c:when test = "${empty(employees)}">
                                    <tr>
                                        <td class = "column_title">Code</td>
                                        <td class = "column_title">Name</td>
                                        <td class = "column_title">EmailId</td>
                                        <td class = "column_title">MobileNo</td>
                                        <td class = "column_title">Date Of Birth</td>
                                        <td class = "column_title">Date Of Joining</td>
                                        <td class = "column_title">Action </td>
                                    </tr>
                                    <tr><td colspan = "7">No Records Found</td></tr>   
                                </c:when>
                                <c:otherwise>
                                    <tr>
                                        <th onclick = "enableSearchBox(0, 'employeeDetails')">Code</th>
                                        <th onclick = "enableSearchBox(1, 'employeeDetails')">Name</th>
                                        <th onclick = "enableSearchBox(2, 'employeeDetails')">EmailId</th>
                                        <th onclick = "enableSearchBox(3, 'employeeDetails')">MobileNo</th>
                                        <th onclick = "enableSearchBox(4, 'employeeDetails')">Date Of Birth</th>
                                        <th onclick = "enableSearchBox(5, 'employeeDetails')">Date Of Joining</th>
                                        <td class = "headings">Action </td>
                                    </tr>
                                    <tr id = "search" class = "hide">
                                        <td colspan = "6"><input type = "text" name = "searchEmpCode" id = "searchEmpCode" onKeyUp = "filterTableData(0)" placeholder = "Search By EmpCode"/></td>
                                        <td colspan = "6"><input type = "text" name = "searchName" id = "searchName" onKeyUp = "filterTableData(1)" placeholder = "Search By Name"/></td>
                                        <td colspan = "6"><input type = "text" name = "searchEmail" id = "searchEmail" onKeyUp = "filterTableData(2)" placeholder = "Search By Email"/></td>
                                        <td colspan = "6"><input type = "text" maxlength = "6" name = "searchMobileNo" id = "searchMobileNo" onKeyUp = "filterTableData(3)" placeholder = "Search By Mobile No"/></td>
                                        <td colspan = "6"><input type = "text" name = "searchDOB" id = "searchDOB" onKeyUp = "filterTableData(4)" placeholder = "Search By DOB"/></td>
                                        <td colspan = "6"><input type = "text" name = "searchDOJ" id = "searchDOJ" onKeyUp = "filterTableData(5)" placeholder = "Search By DOJ"/></td>
                                        <td><img src = "/MANAGEMENT/images/up.png" alt = "upArrow" onclick = "hideSearchBar('employeeDetails')"/></td>
                                    </tr>
                                    <tr id = "no_search_results" class = "hide">
                                        <td colspan = "7">No Search Results Found</td>
                                    </tr>
                                    <c:forEach items = "${employees}" var = "employee">
                                        <tr>
                                            <td>
                                                <a href = "/MANAGEMENT/employeeController?parameter=getEmployee&action=display&empId=${employee.empCode}" class = "tooltip" >${employee.empCode}
                                                    <span class="tooltiptext">Click to view Employee Details</span>
                                                 </a>
                                            </td>
                                            <td>${employee.name}</td>
                                            <td class = "truncate_col">${employee.emailId}</td>
                                            <td>${employee.phoneNo}</td>
                                            <td>${employee.dateOfBirth}</td>
                                            <td>${employee.dateOfJoining}</td>
                                            <td>
                                                <a href = "/MANAGEMENT/employeeController?parameter=getEmployee&action=edit&empId=${employee.empCode}" >Edit</a> | 
                                                <a href = "/MANAGEMENT/employeeController?parameter=deleteEmployee&empId=${employee.empCode}" >Delete</a> |
                                                <a id = "assignProjects" href = "#" onclick = "assignProjects('${employee.empCode}')">Assign</a>
                                            </td>
                                        </tr>    
                                    </c:forEach>
                                </c:otherwise>
                            </c:choose>
                    </table>
                </div>
            </td>
        </tr>
        <tr height = "5%" ><td colspan = "2" ><%@ include file="../../jsp/common/footer.jsp"%></td></tr>
    </table>
    <%@ include file="../../jsp/employee/assignprojectmodal.jsp"%>
</body>
</html>
 
