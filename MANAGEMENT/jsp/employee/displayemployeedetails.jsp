<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<html>
<head>
<link href = "/MANAGEMENT/css/table-style.css" rel = "stylesheet" type = "text/css">
<link href = "/MANAGEMENT/css/tool-tip.css" rel = "stylesheet" type = "text/css">
<script>
var alertMessage = '${message}';
if(alertMessage != "") {
    alert(alertMessage);
}
</script>
</head>
<body>
     <table  border = "0" cellspacing = "0" cellpadding = "0" style = "width:100%;height:100%">
        <tr height = "8%"><td colspan = "2" ><%@ include file="../../jsp/common/header.jsp"%></td></tr>
        <tr height = "87%">
            <td width = "17%"><%@ include file="../../jsp/common/menu.jsp"%></td>
            <td width = "81%">
                <div class = "container">
                    <table border="0" cellpadding = "0" cellspacing = "0" class = "table-position">
                        <tr>
                            <td>
                                <label class = "label">Employee-Detail</label><br><br>
                                <table border="0" cellpadding = "0" cellspacing = "0" class = "table-all">
                                    <tr>
                                        <th>Code</th>
                                        <th>Name</th>
                                        <th>EmailId</th>
                                        <th>MobileNo</th>
                                        <th>Date Of Birth</th>
                                        <th>Date Of Joining</th>
                                    </tr>
                                    <tr>
                                        <td>${employee.empCode}</td>
                                        <td>${employee.name}</td>
                                        <td class = "truncate_col">${employee.emailId}</td>
                                        <td>${employee.phoneNo}</td>
                                        <td>${employee.dateOfBirth}</td>
                                        <td>${employee.dateOfJoining}</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr><td><br></td></tr>
                        <tr>
                            <td>
                                <label class = "label">Address-Details :</label><br><br>
                                <table border="0" cellpadding = "0" cellspacing = "0" class = "table-all">
                                    <tr>
                                        <th>FlatNo</th>
                                        <th>StreetName</th>
                                        <th>City</th>
                                        <th>State</th>
                                        <th>ZipCode</th>
                                        <th>IsPermanent</th>
                                    </tr>
                                    <c:choose>
                                        <c:when test = "${empty(employee.addresses)}">
                                             <tr><td>No Records Found</td></tr>
                                        </c:when>
                                        <c:otherwise>
                                            <c:forEach items = "${employee.addresses}" var = "address">
                                                <tr>
                                                    <td>${address.flatNo}</td>
                                                    <td>${address.streetName}</td>
                                                    <td>${address.city}</td>
                                                    <td>${address.state}</td>
                                                    <td>${address.zipCode}</td>
                                                    <td>${address.isPermanent}</td>
                                                </tr>            
                                            </c:forEach>
                                        </c:otherwise>
                                    </c:choose>
                                </table>
                            </td>
                        </tr>
                        <tr><td><br></td></tr>
                        <tr>
                            <td>
                                <label class = "label">Project-Details :</label> 
                                <span class = "assign_button"><a id = "assignProjects" href = "#" onclick = "assignProjects('${employee.empCode}')">Assign Projects</a></span>
                                <br><br>
                                <table border="0" cellpadding = "0" cellspacing = "0" class = "table-all">
                                    <tr>
                                        <th>Code</th>
                                        <th>Client Name</th>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>Domain</th>
                                    </tr>
                                    <c:choose>
                                        <c:when test = "${empty(employee.projects)}">
                                            <tr><td colspan = "5">No Records Found</td> 
                                        </c:when>
                                        <c:otherwise>
                                            <c:forEach items = "${employee.projects}" var = "project">
                                                <tr>
                                                    <td>
                                                        <a href = "/MANAGEMENT/projectController?parameter=getProject&action=display&projectId=${project.projectCode}" class = "tooltip" >${project.projectCode}
                                                        <span class="tooltiptext">Click to view Project Details</span>
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <a href = "/MANAGEMENT/clientController?parameter=getClient&action=display&clientId=${project.client.clientCode}" class = "tooltip">${project.client.name}
                                                        <span class="tooltiptext">Click to view Client Details</span>
                                                        </a>
                                                    </td>
                                                    <td>${project.title}</td>
                                                    <td class = "truncate_col">${project.description}</td>
                                                    <td>${project.domain}</td>
                                                </tr>            
                                            </c:forEach>
                                        </c:otherwise>
                                    </c:choose>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>  
            </td>
        </tr>
        <tr height = "5%" ><td colspan = "2" ><%@ include file="../../jsp/common/footer.jsp"%></td></tr>
    </table>
    <%@ include file="../../jsp/employee/assignprojectmodal.jsp"%>
</body>
</html>
