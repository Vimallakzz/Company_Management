<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix = "c"%> 
<html>
<head>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<link href = "/MANAGEMENT/css/assignemployeeprojectmodal.css" rel = "stylesheet"/>
</head>
<body>
    <table border = "0" cellpadding = "0" cellspacing = "0" style = "width:100%">
        <tr>
            <td width = "10%"></td>
            <td width = "40%">
                <form name = "assignProjects" target = "_parent" action = "/MANAGEMENT/employeeProjectController" method = "post">
                    <input type = "hidden" name = "parameter" value = "assignProjects"/>
                    <label> EmpCode </label><br>
                    <input type = "text" id = "assign_to_empId" name = "empId" value = "${employee.empCode}" readonly>
                    
                    <label> EmpName </label><br>
                    <input type = "text" id = "assign_to_empName" name = "empName" value = "${employee.name}" readonly>

                    <label>Projects : </label><br>
                    <select multiple name = "projectId" id = "projectId">
                        <c:forEach items = "${projects}" var = "project">
                            <option value = "${project.projectCode}">${project.title}</option>
                        </c:forEach>
                    </select>
                    <input type ="submit" value = "Assign Projects"/> 
                    </form>           
            </td>
            <td width = "20%">
        </tr>
    </table>          
    <script type="text/javascript">
        var empProjects = [];
        <c:forEach items = "${employee.projects}" var = "empProject">
           empProjects.push('${empProject.projectCode}');
        </c:forEach>
        console.log(empProjects);
        $("#projectId").select2().val(empProjects).trigger("change");
        $('#projectId').select2({placeholder:'Select the projects which you want to associate to employee'});
    </script>
</body>
</html>
