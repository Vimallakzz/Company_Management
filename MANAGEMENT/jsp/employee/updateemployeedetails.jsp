<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html>
<head>

<style>
.personal_detail {
    padding-bottom: 13px !important;
}

.temporary_detail_show {
    padding-bottom: 20px;
    visibility : visible;
}

.temporary_detail_hide {
    padding-bottom: 20px;
    visibility : hidden;
}

input[type=text],input[type=number],input[type=email],input[type=date], select {
    width: 70%; 
    padding: 12px;
    margin: 6px 0 16px 0;
    border: 1px solid #ccc;
    border-radius: 10px;
}

input[type=submit] {
    background-color: #4CAF50;
    color: white;
    padding: 12px 107px;
    border: none;
    border-radius: 4px;
    cursor: pointer;
    margin:20px;
}

input[type=submit]:hover {
    background-color: #45a049;
}

.container {
  max-width: -webkit-fill-available;
  width: 100%;
  margin: 0 auto;
  position: relative;
}

</style>
<script>
    function toogleTemporaryAddress() {
    var isChecked = document.getElementById("isSameAsPermanent").checked;
    if (isChecked) {
        document.getElementById("temporaryAddress").className = "temporary_detail_hide";
    } else {
        document.getElementById("temporaryAddress").className = "temporary_detail_show";
    }
    
    }
</script>
</head>
<body>

    <table  border = "0" cellspacing = "0" cellpadding = "0" style = "width:100%;height:100%">
        <tr height = "10%"><td colspan = "2" ><%@ include file="../../jsp/common/header.jsp"%></td></tr>
        <tr height = "85%">
            <td width = "17%"><%@ include file="../../jsp/common/menu.jsp"%></td>
            <td width = "81%">
                <div class = "container">
                    <fieldset>
                        <legend>Employee Details Form</legend>
                        <form action = "/MANAGEMENT/employeeController" method = "get" >
                            <input type = "hidden" name = "parameter" value = "updateEmployeeDetails"/>
                            <input type = "hidden" name = "id" value = "${employee.id}"/>
                            <input type = "hidden" name = "empCode" value = "${employee.empCode}"/>
                            <c:forEach items = "${employee.addresses}" var = "address">
                                <c:choose>
                                    <c:when test = "${address.isPermanent == true}">
                                         <input type = "hidden" name = "permanent_address_id" value = "${address.id}" />
                                    </c:when>
                                    <c:otherwise>
                                         <input type = "hidden" name = "temporary_address_id" value = "${address.id}" />
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                            <table cellpadding = "0" cellspacing = "0" border = "0" style = "width:100%;height:100%">
                                <tr height = "100%">
                                    <td width = "33%" class = "personal_detail">
                                        <label>Personal Details :</label><br><br>
                                        <label>Name :</label><br>
                                        <input type = "text" name = "empName" value = "${employee.name}" id = "empName" placeholder = "Enter the Employee Name"><br>
                                        
                                        <label>Email-Id :</label><br>
                                        <input type = "email" name = "emailId" id = "emailId" value = "${employee.emailId}" placeholder = "Enter the Employee EmailId"><br>
                                        
                                        <label>PhoneNo : </label><br>
                                        <input type = "text" name = "phoneNo" value = "${employee.phoneNo}" id = "phoneNo" placeholder = "Enter Mobile No"><br>
                                        
                                        <label>Date Of Birth :</label><br>
                                        <input type = "date" name = "dateOfBirth" value = "${employee.dateOfBirth}" id = "dateOfBirth"><br>
                                        
                                        <label>Date Of Joining :</label><br>
                                        <input type = "date" name = "dateOfJoining" value = "${employee.dateOfJoining}" id = "dateOfJoining"><br>
                                    </td>
                                    <td width = "33%">
                                        <c:forEach items = "${employee.addresses}" var = "address">
                                            <c:choose>
                                                <c:when test = "${address.isPermanent == true}">
                                                    <label>Permanent Address Details:</label><br><br>
                                                    <label>FlatNo :</label><br>
                                                    <input type = "text" name = "p_flatNo" value = "${address.flatNo}" id = "p_flatNo" placeholder = "Enter the FlatNo No"><br>
                                                    
                                                    <label>StreetName :</label><br>
                                                    <input type = "text" name = "p_streetName" value = "${address.streetName}"  id = "p_streetName" placeholder = "Enter the Street Name"><br>
                                                    <label>State :</label><br> 
                                                    <c:set var = "per_state" value = "${address.state}"/>
                                                    <select name = "p_state"  id = "p_state">
                                                        <option value="" disabled selected>Select your option</option>
                                                        <option value = "TamilNadu" >TamilNadu</option>
                                                    </select><br>
                                                   
                                                    <label>City:</label><br>
                                                    <c:set var = "per_city" value = "${address.city}"/>
                                                    <select name = "p_city"  id = "p_city">
                                                        <option value="" disabled selected>Select your option</option>
                                                        <option value = "chennai">chennai</option>
                                                        <option value = "coimbatore">coimbatore</option>
                                                        <option value = "villupuram">villupuram</option>
                                                    </select><br>
                                                   
                                                    <label>ZipCode :</label><br>
                                                    <input type = "text" name = "p_zipCode" value = "${address.zipCode}"  id = "p_zipCode" placeholder = "Enter the Pincode No"><br>
                                                </c:when>
                                            </c:choose>
                                        </c:forEach>
                                <c:if test = "${fn:length(employee.addresses) == 1}">
                                    <input type = "checkbox" checked name = "isSameAsPermanent" value = "yes" id = "isSameAsPermanent" onclick = "toogleTemporaryAddress()">is Current Address same As Permanent </input>
                                    </td>
                                    <td width = "33%" id = "temporaryAddress" class = "temporary_detail_hide">
                                        <label>Temporary Address Details:</label><br><br>
                                        <label>FlatNo :</label><br>
                                        <input type = "text" name = "t_flatNo" id = "t_flatNo" placeholder = "Enter the FlatNo No"><br>
                                        <label>StreetName :</label><br>
                                        <input type = "text" name = "t_streetName" id = "t_streetName" placeholder = "Enter the Street Name"><br>
                                        <label>State :</label><br>
                                        <select name = "t_state" id = "t_state">
                                            <option value="" disabled selected>Select your option</option>
                                            <option value = "TamilNadu">TamilNadu</option>
                                        </select><br>
                                                  
                                        <label>City:</label><br>
                                        <select name = "t_city" id = "t_city">
                                            <option value="" disabled selected>Select your option</option>
                                            <option value = "chennai">chennai</option>
                                            <option value = "coimbatore">coimbatore</option>
                                            <option value = "villupuram">villupuram</option>
                                        </select><br>
                                                   
                                        <label>ZipCode :</label><br>
                                        <input type = "text" name = "t_zipCode" id = "t_zipCode" placeholder = "Enter the Pincode No">
                                    </td>
                                </c:if>
                                <c:if test = "${fn:length(employee.addresses) == 2}">
                                    <input type = "checkbox" name = "isSameAsPermanent" value = "yes" id = "isSameAsPermanent" onclick = "toogleTemporaryAddress()">is Current Address same As Permanent </input>
                                    </td>
                                    <td width = "33%" id = "temporaryAddress" class = "temporary_detail_show">
                                        <c:forEach items = "${employee.addresses}" var = "address">
                                            <c:choose>
                                                <c:when test = "${address.isPermanent == false}">  
                                                    <label>Temporary Address Details:</label><br><br>
                                                    <label>FlatNo :</label><br>
                                                    <input type = "text" name = "t_flatNo" value = "${address.flatNo}" id = "t_flatNo" placeholder = "Enter the FlatNo No"><br>
                                                        
                                                    <label>StreetName :</label><br>
                                                    <input type = "text" name = "t_streetName" value = "${address.streetName}" id = "t_streetName" placeholder = "Enter the Street Name"><br>
                                                    
                                                    <label>State :</label><br>
                                                    <c:set var = "tem_state" value = "${address.state}"/>
                                                    <select name = "t_state" id = "t_state">
                                                        <option value="" disabled selected>Select your option</option>
                                                        <option value = "TamilNadu">TamilNadu</option>
                                                    </select><br>
                                                       
                                                    <label>City:</label><br>
                                                    <c:set var = "tem_city" value = "${address.city}"/>
                                                    <select name = "t_city" id = "t_city">
                                                        <option value="" disabled selected>Select your option</option>
                                                        <option value = "chennai">chennai</option>
                                                        <option value = "coimbatore">coimbatore</option>
                                                        <option value = "villupuram">villupuram</option>
                                                    </select><br>
                                                       
                                                    <label>ZipCode :</label><br>
                                                    <input type = "text" name = "t_zipCode" value = "${address.zipCode}" id = "t_zipCode" placeholder = "Enter the Pincode No">
                                                </c:when>                                                   
                                            </c:choose>
                                        </c:forEach>
                                    </td>
                                </c:if>       
                                </tr>
                                <tr>
                                    <td width = "33%"/>
                                    <td width = "33%"><input type="submit" value="Update"></td>
                                </tr> 
                            </table>
                            
                        </form> 
                    </fieldset>
                </div>
            </td>
        </tr>
        <tr height = "5%" ><td colspan = "2" ><%@ include file="../../jsp/common/footer.jsp"%></td></tr>
    </table>
    <script>  
        document.getElementById("p_city").value = "${per_city}";
        document.getElementById("t_city").value = "${tem_city}";
        document.getElementById("p_state").value = "${per_state}";
        document.getElementById("t_state").value = "${tem_state}";
    </script>
</body>
</html>


