<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<html>
<head>
<title></title>
<link href = "/MANAGEMENT/css/table-style.css" rel = "stylesheet" type = "text/css">
<link href = "/MANAGEMENT/css/table-style-search.css" rel = "stylesheet" type = "text/css">
<link href = "/MANAGEMENT/css/tool-tip.css" rel = "stylesheet" type = "text/css">
<script src = "/MANAGEMENT/js/search.js"></script>
<script src = "/MANAGEMENT/js/client-filter-data.js"></script>
<script>
    var alertMessage = '${message}';
    if(alertMessage != "") {
        alert(alertMessage);
    }
</script>
</head>
<body>
    <table  border = "0" cellspacing = "0" cellpadding = "0" style = "width:100%;height:100%">
        <tr height = "8%"><td colspan = "2" ><%@ include file="../../jsp/common/header.jsp"%></td></tr>
        <tr height = "87%">
            <td width = "17%"><%@ include file="../../jsp/common/menu.jsp"%></td>
            <td width = "81%">
               <div class = "container-all">
                    <label class = "label">clients-List :</label><br><br>
                        <table id = "clientDetails" border = "0" cellspacing = "0" cellpadding = "0" class = "table-all table-position">
                            <c:choose>
                                <c:when test = "${empty(clients)}">
                                    <tr>
                                        <td class = "column_title">Code</td>
                                        <td class = "column_title">Name</td>
                                        <td class = "column_title">EmailId</td>
                                        <td class = "column_title">MobileNo</td>
                                        <td class = "column_title">Action</td>
                                    </tr>
                                    <tr><td colspan = "7">No Records Found</td></tr>   
                                </c:when>
                                <c:otherwise>
                                    <tr>
                                        <th onclick = "enableSearchBox(0, 'clientDetails')">Code</th>
                                        <th onclick = "enableSearchBox(1, 'clientDetails')">Name</th>
                                        <th onclick = "enableSearchBox(2, 'clientDetails')">EmailId</th>
                                        <th onclick = "enableSearchBox(3, 'clientDetails')">MobileNo</th>
                                        <td class = "headings">Action</td>
                                    </tr>
                                    <tr id = "search" class = "hide">
                                        <td colspan = "4"><input type = "text" name = "searchclientCode" id = "searchclientCode" onKeyUp = "filterTableData(0)" placeholder = "Search By Code"/></td>
                                        <td colspan = "4"><input type = "text"  name = "searchName" id = "searchName" onKeyUp = "filterTableData(1)" placeholder = "Search By Client Name"/></td>
                                        <td colspan = "4"><input type = "text" name = "searchEmail" id = "searchEmail" onKeyUp = "filterTableData(2)" placeholder = "Search By Email"/></td>
                                        <td colspan = "4"><input type = "text" name = "searchMobileNo" id = "searchMobileNo" onKeyUp = "filterTableData(3)" placeholder = "Search By Mobile No"/></td>
                                        <td><img src = "/MANAGEMENT/images/up.png" alt = "upArrow" onclick = "hideSearchBar('clientDetails')"/></td>
                                    </tr>
                                    <tr id = "no_search_results" class = "hide">
                                        <td colspan = "7">No Search Results Found</td>
                                    </tr>
                                    <c:forEach items = "${clients}" var = "client">
                                        <tr>
                                            <td>
                                                <a href = "/MANAGEMENT/clientController?parameter=getClient&action=display&clientId=${client.clientCode}" class = "tooltip" >${client.clientCode}
                                                <span class="tooltiptext">Click to view Client Details</span>    
                                                </a>
                                            </td>
                                            <td>${client.name}</td>
                                            <td class = "truncate_col">${client.emailId}</td>
                                            <td>${client.phoneNo}</td>
                                            <td>
                                                <a href = "/MANAGEMENT/clientController?parameter=getClient&action=edit&clientId=${client.clientCode}" >Edit</a> | 
                                                <a href = "/MANAGEMENT/clientController?parameter=deleteClient&clientId=${client.clientCode}" >Delete</a>
                                            </td>
                                        </tr>    
                                    </c:forEach>
                                </c:otherwise>
                            </c:choose>
                    </table>
                </div>
            </td>
        </tr>
        <tr height = "5%" ><td colspan = "2" ><%@ include file="../../jsp/common/footer.jsp"%></td></tr>
    </table>
</body>
</html>
