<html>
<head>
<link href = "/MANAGEMENT/css/modal.css" rel = "stylesheet" type = "text/css">
</head>
<body>
<div id="modal" class="modal">
    <div class="modal-content">
        <div>
            <div>
                <span class = "span_align">Assign Employees</span>
                <span id = "closeModal" class="close">&#10008;</span>
            </div>
            <div>
                <iframe id = "myFrame" src = "" style="border:none;"></iframe>
            </div>
        </div>    
    </div>
</div>
<script src = "/MANAGEMENT/js/assignemployeemodal.js"></script>
</body>
</html>
