<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<html>
<head>
<link href = "/MANAGEMENT/css/table-style.css" rel = "stylesheet" type = "text/css">
<link href = "/MANAGEMENT/css/tool-tip.css" rel = "stylesheet" type = "text/css">
<script>
    var alertMessage = '${message}';
    if(alertMessage != "") {
        alert(alertMessage);
    }
</script>
</head>
<body>
     <table  border = "0" cellspacing = "0" cellpadding = "0" style = "width:100%;height:100%">
        <tr height = "8%"><td colspan = "2" ><%@ include file="../../jsp/common/header.jsp"%></td></tr>
        <tr height = "87%">
            <td width = "17%"><%@ include file="../../jsp/common/menu.jsp"%></td>
            <td width = "81%">
                <div class = "container">
                    <table border="0" cellpadding = "0" cellspacing = "0" class = "table-position">
                        <tr>
                            <td>
                                <label class = "label">Project-Details :</label><br><br>
                                <table border="0" cellpadding = "0" cellspacing = "0" class = "table-all">
                                    <tr>
                                        <th>Project Code</th>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>Domain</th>
                                    </tr>
                                    <tr>
                                        <td>${project.projectCode}</td>
                                        <td>${project.title}</td>
                                        <td>${project.description}</td>
                                        <td>${project.domain}</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr><td><br></td></tr>
                        <tr>
                            <td>
                                <label class = "label">Client Details :</label><br><br>
                                <table border="0" cellpadding = "0" cellspacing = "0" class = "table-all">
                                        <tr>
                                            <th>Code</th>
                                            <th>Name</th>
                                            <th>EmailId</th>
                                            <th>MobileNo</th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href = "/MANAGEMENT/clientController?parameter=getClient&action=display&clientId=${project.client.clientCode}" class = "tooltip">${project.client.clientCode}
                                                    <span class="tooltiptext">Click to view Client Details</span>
                                                </a>
                                            </td>
                                            <td>${project.client.name}</td>
                                            <td class = "truncate_col">${project.client.emailId}</td>
                                            <td>${project.client.phoneNo}</td>
                                        </tr>
                                </table>
                            </td>
                        </tr>
                        <tr><td><br></td></tr>
                        <tr>
                            <td>
                                <label class = "label">Employee-Details :</label>
                                <span class = "assign_button"><a id = "assignEmployees" href = "#" onclick = "assignEmployees('${project.projectCode}')">Assign Employees</a></span>
                                <br><br>
                                <table border="0" cellpadding = "0" cellspacing = "0" class = "table-all">
                                    <tr>
                                        <th>Code</th>
                                        <th>Name</th>
                                        <th>EmailId</th>
                                        <th>Mobile No</th>
                                        <th>DOB</th>
                                        <th>DOJ</th>
                                    </tr>
                                    <c:choose>
                                        <c:when test = "${empty(project.employees)}">
                                            <tr><td colspan = "6">No Records Found</td> 
                                        </c:when>
                                        <c:otherwise>
                                            <c:forEach items = "${project.employees}" var = "employee">
                                                <tr>
                                                    <td>
                                                        <a href = "/MANAGEMENT/employeeController?parameter=getEmployee&action=display&empId=${employee.empCode}" class = "tooltip">${employee.empCode}
                                                        <span class="tooltiptext">Click to view Employee Details</span>
                                                        </a>
                                                    </td>
                                                    <td>${employee.name}</td>
                                                    <td class = "truncate_col">${employee.emailId}</td>
                                                    <td>${employee.phoneNo}</td>
                                                    <td>${employee.dateOfBirth}</td>
                                                    <td>${employee.dateOfJoining}</td>
                                                </tr>            
                                            </c:forEach>
                                        </c:otherwise>
                                    </c:choose>
                                </table>
                            </td>
                        </tr>
                    </table>              
                </div>
            </td>
        </tr>
        <tr height = "5%" ><td colspan = "2" ><%@ include file="../../jsp/common/footer.jsp"%></td></tr>
    </table>
    <%@ include file="../../jsp/project/assignemployeemodal.jsp"%>
</body>
</html>
