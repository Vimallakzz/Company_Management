<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix = "c"%> 
<html>
<head>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<link href = "/MANAGEMENT/css/assignemployeeprojectmodal.css" rel = "stylesheet"/>
</head>
<body>
    <table border = "0" cellpadding = "0" cellspacing = "0" style = "width:100%">
        <tr>
            <td width = "10%"></td>
            <td width = "40%">
                <form name = "assignEmployees" target = "_parent" action = "/MANAGEMENT/employeeProjectController" method = "post">
                    <input type = "hidden" name = "parameter" value = "assignEmployees"/>
                    <label> ProjectCode </label><br>
                    <input type = "text" id = "assign_to_projectId" name = "projectId" value = "${project.projectCode}" readonly>
                    <label> ProjectName </label><br>
                    <input type = "text" id = "assign_to_title" name = "title" value = "${project.title}" readonly>

                    <label>Employees : </label><br>
                    <select multiple name = "empId" id = "empId">
                        <c:forEach items = "${employees}" var = "employee">
                            <option value = "${employee.empCode}">${employee.name}</option>
                        </c:forEach>
                    </select>
                    
                    <input type = "submit" value = "Assign Employees" /> 
                </form>                    
            </td>
            <td width = "20%">
        </tr>
    </table>          
    <script type="text/javascript">
        var projemployees = [];
        <c:forEach items = "${project.employees}" var = "projEmployee">
           projemployees.push('${projEmployee.empCode}');
        </c:forEach>
        $("#empId").select2().val(projemployees).trigger("change");
        $("#empId").select2({placeholder:'Select the empoloyee which you want to associate to project'});
    </script>
</body>
</html>
