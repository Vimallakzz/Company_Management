<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<html>
<head>
<title></title>
<link href = "/MANAGEMENT/css/table-style.css" rel = "stylesheet" type = "text/css">
<link href = "/MANAGEMENT/css/table-style-search.css" rel = "stylesheet" type = "text/css">
<link href = "/MANAGEMENT/css/tool-tip.css" rel = "stylesheet" type = "text/css">
<script src = "/MANAGEMENT/js/search.js"></script>
<script src = "/MANAGEMENT/js/project-filter-data.js"></script>
<script>
var alertMessage = '${message}';
if(alertMessage != "") {
    alert(alertMessage);
}
</script>
</head>
<body>
    <table  border = "0" cellspacing = "0" cellpadding = "0" style = "width:100%;height:100%">
        <tr height = "8%"><td colspan = "2" ><%@ include file="../../jsp/common/header.jsp"%></td></tr>
        <tr height = "87%">
            <td width = "17%"><%@ include file="../../jsp/common/menu.jsp"%></td>
            <td width = "81%">
               <div class = "container-all">
                    <label class = "label">project-List :</label><br><br>
                        <table id = "projectDetails" border = "0" cellspacing = "0" cellpadding = "0" class = "table-all table-position">
                            <c:choose>
                                <c:when test = "${empty(projects)}">
                                    <tr>
                                        <td class = "column_title">Project Code</td>
                                        <td class = "column_title">Client Name</td>
                                        <td class = "column_title">Title</td>
                                        <td class = "column_title">Description</td>
                                        <td class = "column_title">Domain</td>
                                        <td class = "column_title">Action</td>
                                    </tr>
                                    <tr><td colspan = "6">No Records Found</td></tr>   
                                </c:when>
                                <c:otherwise>
                                    <tr>
                                        <th onclick = "enableSearchBox(0, 'projectDetails')">Project Code</th>
                                        <th onclick = "enableSearchBox(1, 'projectDetails')">Client Name</th>
                                        <td class = "headings">Title</td>
                                        <td class = "headings">Description</td>
                                        <th onclick = "enableSearchBox(2, 'projectDetails')">Domain</th>
                                        <td class = "headings">Action</th>
                                    </tr>
                                    <tr id = "search" class = "hide">
                                        <td colspan = "5"><input type = "text" name = "searchProjectCode" id = "searchProjectCode" onKeyUp = "filterTableData(0)" placeholder = "Search By Project Code"/></td>
                                        <td colspan = "5"><input type = "text" name = "searchClientName" id = "searchClientName" onKeyUp = "filterTableData(1)" placeholder = "Search By Client Name"/></td>
                                        <td colspan = "5"><input type = "text" name = "searchProjectDomains" id = "searchProjectDomains" onKeyUp = "filterTableData(4)" placeholder = "Search By Domain"/></td>
                                        <td><img src = "/MANAGEMENT/images/up.png" alt = "upArrow" onclick = "hideSearchBar('projectDetails')"/></td>
                                    </tr>
                                    <tr id = "no_search_results" class = "hide">
                                        <td colspan = "7">No Search Results Found</td>
                                    </tr>
                                    <c:forEach items = "${projects}" var = "project">
                                        <tr>
                                            <td>
                                                <a href = "/MANAGEMENT/projectController?parameter=getProject&action=display&projectId=${project.projectCode}" class = "tooltip">${project.projectCode}
                                                    <span class="tooltiptext">Click to view Project Details</span>
                                                </a>
                                            </td>
                                            <td>${project.client.name}</td>
                                            <td>${project.title}</td>
                                            <td class = "truncate_col">${project.description}</td>
                                            <td>${project.domain}</td>
                                            <td>
                                                <a href = "/MANAGEMENT/projectController?parameter=getProject&action=edit&projectId=${project.projectCode}" >Edit</a> | 
                                                <a href = "/MANAGEMENT/projectController?parameter=deleteProject&projectId=${project.projectCode}" >Delete</a> |
                                                <a id = "assignEmployees" href = "#" onclick = "assignEmployees('${project.projectCode}')">Assign</a>
                                            </td>
                                        </tr>    
                                    </c:forEach>
                                </c:otherwise>
                            </c:choose>
                    </table>
                </div>
            </td>
        </tr>
        <tr height = "5%" ><td colspan = "2" ><%@ include file="../../jsp/common/footer.jsp"%></td></tr>
    </table>
    <%@ include file="../../jsp/project/assignemployeemodal.jsp"%>
</body>
</html>
