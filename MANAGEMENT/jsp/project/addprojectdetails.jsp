<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<html>
<head>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<style>
.project_detail {
    padding-bottom: 13px !important;
}


input[type=text], select{
    width: 100%; 
    padding: 12px;
    margin: 6px 0 16px 0;
    border: 1px solid #ccc;
    border-radius: 10px;
    
} 

textarea{
    width: 100%; 
    padding: 0px;
    margin: 6px 0 16px 0;
    border: 1px solid #ccc;
    border-radius: 10px;
    
} 

.select2-selection--multiple {
    border-radius: 10px !important;
    margin: 6px 0 0px 0 !important;
    background-color: #eae5e5 !important;
}

.select2-selection--single {
    border-radius: 10px !important;
    height : 38px !important;
    margin: 6px 0 0px 0 !important;
    background-color: #eae5e5 !important;
} 

.select2-selection__arrow {
    height: 48px !important;
}

.select2-selection__rendered {
    line-height: 33px !important;
}

input[type=submit] {
    background-color: #4CAF50;
    color: white;
    padding: 12px 107px;
    border: none;
    border-radius: 4px;
    cursor: pointer;
    width: 100%;
}

input[type=submit]:hover {
    background-color: #45a049;
}

.container {
  padding-bottom: 115px;
}

</style>
</head>
<body >
    <table  border = "0" cellspacing = "0" cellpadding = "0" style = "width:100%;height:100%">
        <tr height = "10%"><td colspan = "2" ><%@ include file="../../jsp/common/header.jsp"%></td></tr>
        <tr height = "85%">
           <td width = "17%"><%@ include file="../../jsp/common/menu.jsp"%></td>
           <td width = "81%">
                <div class = "container">
                    <fieldset>
                        <legend>Project Details Form</legend>
                        <form action = "/MANAGEMENT/projectController" method = "get" >
                            <input type = "hidden" name = "parameter" value = "addProjectDetails"/> 
                            <table cellpadding = "0" cellspacing = "0" border = "0" style = "width:100%;height:100%">
                                <tr height = "100%">
                                    <td width = "30%"/>
                                    <td width = "40%" class = "project_detail">
                                        <label>Client Name</label><br>
                                        <select name = "clientId" id = "clientName">
                                            <option disabled selected value = "">Select The Client Name Which You want to add Project</option>
                                            <c:forEach items = "${clients}" var = "client">
                                                <option  value = "${client.clientCode}">${client.name}</option>
                                            </c:forEach>
                                        </select><br> 
                                        <label>Project Title :</label><br>
                                        <input type = "text" name = "projectTitle" id = "projectTitle" placeholder = "Enter the project Title"><br>
                                        
                                        <label>Project Description :</label><br>
                                        <textarea name = "projectDescription" rows="5" cols="50"></textarea>
                                        <label>Project Domain :</label><br>
                                        <select multiple name = "projectDomain" id = "projectDomain">
                                            <option value = "On-premise">Web Application(On-premise)</option>
                                            <option value = "Cloud">Web Application(Cloud)</option>
                                            <option value = "Android">Android App development</option>
                                            <option value = "Ios">IOS App development</option>
                                        </select>
                                    </td>
                                    <td width = "30%"/>
                                </tr>
                                <tr>
                                    <td width = "30%"/>
                                    <td width = "40%"><input type="submit" value="Submit"></td>
                                </tr> 
                            </table>
                            
                        </form> 
                    </fieldset>
                </div>
            </td>
        </tr>
        <tr height = "5%" ><td colspan = "2" ><%@ include file="../../jsp/common/footer.jsp"%></td></tr>
    </table>
    <script type="text/javascript">
        $("#clientName").select2();
        $("#projectDomain").select2({placeholder: "Select domain Which You want to create Project"});
    </script>
</body>
</html>


